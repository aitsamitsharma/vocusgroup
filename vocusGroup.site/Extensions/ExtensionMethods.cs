﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco.Web.Models;

namespace VocusGroup.site.Extensions
{
    public static class ExtensionMethods
    {
        private static UmbracoHelper Umbraco
        {
            get { return new UmbracoHelper(UmbracoContext.Current); }
        }

        #region ImageCropDataSet - Methods

        public static string GetResponsiveCropUrl(
            this ImageCropDataSet cropDataSet,
            int width,
            int height
            )
        {
            return cropDataSet.GetCropUrl(
                width: width,
                height: height,
                quality: 90,
                ratioMode: ImageCropRatioMode.Height,
                furtherOptions: "&slimmage=true"
            );
        }

        public static string GetCropUrl(
            this ImageCropDataSet cropDataSet,
            int? width = null,
            int? height = null,
            string cropAlias = null,
            int? quality = null,
            ImageCropMode? imageCropMode = null,
            ImageCropAnchor? imageCropAnchor = null,
            bool preferFocalPoint = false,
            bool useCropDimensions = false,
            string cacheBusterValue = null,
            string furtherOptions = null,
            ImageCropRatioMode? ratioMode = null,
            bool upScale = true
        )
        {
            var imageResizerUrl = new StringBuilder();

            imageResizerUrl.Append(cropDataSet.Src);

            if (imageCropMode == ImageCropMode.Crop || imageCropMode == null)
            {
                var crop = cropDataSet.GetCrop(cropAlias);

                var cropBaseUrl = cropDataSet.GetCropBaseUrl(cropAlias, preferFocalPoint);
                if (cropBaseUrl != null)
                {
                    imageResizerUrl.Append(cropBaseUrl);
                }
                else
                {
                    return null;
                }

                if (crop != null & useCropDimensions)
                {
                    width = crop.Width;
                    height = crop.Height;
                }
            }
            else
            {
                imageResizerUrl.Append("?mode=" + imageCropMode.ToString().ToLower());

                if (imageCropAnchor != null)
                {
                    imageResizerUrl.Append("&anchor=" + imageCropAnchor.ToString().ToLower());
                }
            }

            if (quality != null)
            {
                imageResizerUrl.Append("&quality=" + quality);
            }

            if (width != null && ratioMode != ImageCropRatioMode.Width)
            {
                imageResizerUrl.Append("&width=" + width);
            }

            if (height != null && ratioMode != ImageCropRatioMode.Height)
            {
                imageResizerUrl.Append("&height=" + height);
            }

            if (ratioMode == ImageCropRatioMode.Width && height != null)
            {
                //if only height specified then assume a sqaure
                if (width == null)
                {
                    width = height;
                }
                var widthRatio = (decimal)width / (decimal)height;
                imageResizerUrl.Append("&widthratio=" + widthRatio.ToString(CultureInfo.InvariantCulture));
            }

            if (ratioMode == ImageCropRatioMode.Height && width != null)
            {
                //if only width specified then assume a sqaure
                if (height == null)
                {
                    height = width;
                }
                var heightRatio = (decimal)height / (decimal)width;
                imageResizerUrl.Append("&heightratio=" + heightRatio.ToString(CultureInfo.InvariantCulture));
            }

            if (upScale == false)
            {
                imageResizerUrl.Append("&upscale=false");
            }

            if (furtherOptions != null)
            {
                imageResizerUrl.Append(furtherOptions);
            }

            if (cacheBusterValue != null)
            {
                imageResizerUrl.Append("&rnd=").Append(cacheBusterValue);
            }

            return imageResizerUrl.ToString();
        }

        public static ImageCropData GetCrop(this ImageCropDataSet dataset, string cropAlias)
        {
            if (dataset == null || dataset.Crops == null || !dataset.Crops.Any())
                return null;

            return dataset.Crops.GetCrop(cropAlias);
        }

        public static ImageCropData GetCrop(this IEnumerable<ImageCropData> dataset, string cropAlias)
        {
            if (dataset == null || !dataset.Any())
                return null;

            if (string.IsNullOrEmpty(cropAlias))
                return dataset.FirstOrDefault();

            return dataset.FirstOrDefault(x => x.Alias.ToLowerInvariant() == cropAlias.ToLowerInvariant());
        }

        public static string GetCropBaseUrl(this ImageCropDataSet cropDataSet, string cropAlias, bool preferFocalPoint)
        {
            var cropUrl = new StringBuilder();

            var crop = cropDataSet.GetCrop(cropAlias);

            // if crop alias has been specified but not found in the Json we should return null
            if (string.IsNullOrEmpty(cropAlias) == false && crop == null)
            {
                return null;
            }
            if ((preferFocalPoint && cropDataSet.HasFocalPoint()) || (crop != null && crop.Coordinates == null && cropDataSet.HasFocalPoint()) || (string.IsNullOrEmpty(cropAlias) && cropDataSet.HasFocalPoint()))
            {
                cropUrl.Append("?center=" + cropDataSet.FocalPoint.Top.ToString(CultureInfo.InvariantCulture) + "," + cropDataSet.FocalPoint.Left.ToString(CultureInfo.InvariantCulture));
                cropUrl.Append("&mode=crop");
            }
            else if (crop != null && crop.Coordinates != null)
            {
                cropUrl.Append("?crop=");
                cropUrl.Append(crop.Coordinates.X1.ToString(CultureInfo.InvariantCulture)).Append(",");
                cropUrl.Append(crop.Coordinates.Y1.ToString(CultureInfo.InvariantCulture)).Append(",");
                cropUrl.Append(crop.Coordinates.X2.ToString(CultureInfo.InvariantCulture)).Append(",");
                cropUrl.Append(crop.Coordinates.Y2.ToString(CultureInfo.InvariantCulture));
                cropUrl.Append("&cropmode=percentage");
            }
            else
            {
                cropUrl.Append("?anchor=center");
                cropUrl.Append("&mode=crop");
            }
            return cropUrl.ToString();
        }

        #endregion

        #region Validation

        /// <summary>
        /// Checks if the e-mail is valid.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool IsValidEmail(this UmbracoHelper umbraco, string email)
        {
            Regex r = new Regex(@"([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})");
            return r.IsMatch(email);
        }

        #endregion

        #region Error

        /// <summary>
        /// Log an exception and send an email.
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="nodeId"></param>
        /// <param name="type"></param>
        public static void LogException<T>(this UmbracoHelper umbraco, Exception ex)
        {
            try
            {
                int nodeId = -1;
                if (System.Web.HttpContext.Current.Items["pageID"] != null)
                {
                    int.TryParse(System.Web.HttpContext.Current.Items["pageID"].ToString(), out nodeId);
                }

                StringBuilder comment = new StringBuilder();
                StringBuilder commentHtml = new StringBuilder();

                commentHtml.AppendFormat("<p><strong>Url:</strong><br/>{0}</p>", System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
                commentHtml.AppendFormat("<p><strong>Node id:</strong><br/>{0}</p>", nodeId);

                //Add the exception.
                comment.AppendFormat("Exception: {0} - StackTrace: {1}", ex.Message, ex.StackTrace);
                commentHtml.AppendFormat("<p><strong>Exception:</strong><br/>{0}</p>", ex.Message);
                commentHtml.AppendFormat("<p><strong>StackTrace:</strong><br/>{0}</p>", ex.StackTrace);

                if (ex.InnerException != null)
                {
                    //Add the inner exception.
                    comment.AppendFormat("- InnerException: {0} - InnerStackTrace: {1}", ex.InnerException.Message, ex.InnerException.StackTrace);
                    commentHtml.AppendFormat("<p><strong>InnerException:</strong><br/>{0}</p>", ex.InnerException.Message);
                    commentHtml.AppendFormat("<p><strong>InnerStackTrace:</strong><br/>{1}</p>", ex.InnerException.StackTrace);
                }

                //Log the Exception into Umbraco.
                LogHelper.Error<T>(comment.ToString(), ex);

                var errorFrom = ConfigurationManager.AppSettings["errorFrom"];
                var errorFromName = ConfigurationManager.AppSettings["errorFromName"];
                var errorTo = ConfigurationManager.AppSettings["errorTo"];

                if (!string.IsNullOrWhiteSpace(errorFrom) && !string.IsNullOrWhiteSpace(errorFromName) && !string.IsNullOrWhiteSpace(errorTo))
                {
                    //Send an email with the exception.
                //    umbraco.SendEmail(
                //        errorFrom,
                //        errorFromName,
                //        errorTo,
                //        "Error log",
                //        commentHtml.ToString());
                }
            }
            catch
            {
                //Do nothing because nothing can be done with this exception.
            }
        }

        #endregion

        #region Other

        /// <summary>
        /// Removes the starting and ending paragraph tags in a string.
        /// </summary>
        /// <param name="u"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static HtmlString RemoveFirstParagraphTag(this UmbracoHelper u, IHtmlString text)
        {
            return new HtmlString(umbraco.library.RemoveFirstParagraphTag(text.ToString()));
        }

        /// <summary>
        /// Appends or updates a query string value to the current Url
        /// </summary>
        /// <param name="key">The query string key</param>
        /// <param name="value">The query string value</param>
        /// <returns>The updated Url</returns>
        public static string AppendOrUpdateQueryString(this UmbracoHelper umbraco, string key, string value)
        {
            return umbraco.AppendOrUpdateQueryString(HttpContext.Current.Request.RawUrl, key, value);
        }

        /// <summary>
        /// Appends or updates a query string value to supplied Url
        /// </summary>
        /// <param name="url">The Url to update</param>
        /// <param name="key">The query string key</param>
        /// <param name="value">The query string value</param>
        /// <returns>The updated Url</returns>
        public static string AppendOrUpdateQueryString(this UmbracoHelper umbraco, string url, string key, string value)
        {
            var q = '?';

            if (url.IndexOf(q) == -1)
            {
                return string.Concat(url, q, key, '=', HttpUtility.UrlEncode(value));
            }

            var baseUrl = url.Substring(0, url.IndexOf(q));
            var queryString = url.Substring(url.IndexOf(q) + 1);
            var match = false;
            var kvps = HttpUtility.ParseQueryString(queryString);

            foreach (var queryStringKey in kvps.AllKeys)
            {
                if (queryStringKey == key)
                {
                    kvps[queryStringKey] = value;
                    match = true;
                    break;
                }
            }

            if (!match)
            {
                kvps.Add(key, value);
            }

            return string.Concat(baseUrl, q, ConstructQueryString(kvps, null, false));
        }

        /// <summary>
        /// Constructs a NameValueCollection into a query string.
        /// </summary>
        /// <remarks>Consider this method to be the opposite of "System.Web.HttpUtility.ParseQueryString"</remarks>
        /// <param name="parameters">The NameValueCollection</param>
        /// <param name="delimiter">The String to delimit the key/value pairs</param>
        /// <param name="omitEmpty">Boolean to chose whether to omit empty values</param>
        /// <returns>A key/value structured query string, delimited by the specified String</returns>
        /// <example>
        /// http://blog.leekelleher.com/2009/09/19/how-to-convert-namevaluecollection-to-a-query-string-revised/
        /// </example>
        private static string ConstructQueryString(NameValueCollection parameters, string delimiter, bool omitEmpty)
        {
            if (string.IsNullOrEmpty(delimiter))
                delimiter = "&";

            var equals = '=';
            var items = new List<string>();

            for (var i = 0; i < parameters.Count; i++)
            {
                foreach (var value in parameters.GetValues(i))
                {
                    var addValue = omitEmpty ? !string.IsNullOrEmpty(value) : true;
                    if (addValue)
                    {
                        items.Add(string.Concat(parameters.GetKey(i), equals, HttpUtility.UrlEncode(value)));
                    }
                }
            }

            return string.Join(delimiter, items.ToArray());
        }

        /// <summary>
        /// Disable the cache of a page.
        /// </summary>
        public static void DisableCache(this UmbracoHelper umbraco)
        {
            HttpContext.Current.Response.AddHeader("pragma", "no-cache");
            HttpContext.Current.Response.AddHeader("cache-control", "private");
            HttpContext.Current.Response.CacheControl = "no-cache";
            HttpContext.Current.Response.Cache.SetExpires(DateTime.Now.AddMinutes(-1));
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetNoStore();
        }

        #endregion

        #region Other object - Methods

        //public static string Target(this Meta meta)
        //{
        //    return meta.NewWindow ? "_blank" : "_self";
        //}

        #endregion
    }
}