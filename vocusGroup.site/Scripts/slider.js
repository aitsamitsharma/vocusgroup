﻿window.Modernizr = function (a, b, c) {
    function d(a) {
        t.cssText = a
    }

    function e(a, b) {
        return d(x.join(a + ";") + (b || ""))
    }

    function f(a, b) {
        return typeof a === b
    }

    function g(a, b) {
        return !!~("" + a).indexOf(b)
    }

    function h(a, b) {
        for (var d in a) {
            var e = a[d];
            if (!g(e, "-") && t[e] !== c) return "pfx" == b ? e : !0
        }
        return !1
    }

    function i(a, b, d) {
        for (var e in a) {
            var g = b[a[e]];
            if (g !== c) return d === !1 ? a[e] : f(g, "function") ? g.bind(d || b) : g
        }
        return !1
    }

    function j(a, b, c) {
        var d = a.charAt(0).toUpperCase() + a.slice(1),
          e = (a + " " + z.join(d + " ") + d).split(" ");
        return f(b, "string") || f(b, "undefined") ? h(e, b) : (e = (a + " " + A.join(d + " ") + d).split(" "), i(e, b, c))
    }

    function k() {
        o.input = function (c) {
            for (var d = 0, e = c.length; e > d; d++) E[c[d]] = !!(c[d] in u);
            return E.list && (E.list = !(!b.createElement("datalist") || !a.HTMLDataListElement)), E
        }("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")), o.inputtypes = function (a) {
            for (var d, e, f, g = 0, h = a.length; h > g; g++) u.setAttribute("type", e = a[g]), d = "text" !== u.type, d && (u.value = v, u.style.cssText = "position:absolute;visibility:hidden;", /^range$/.test(e) && u.style.WebkitAppearance !== c ? (q.appendChild(u), f = b.defaultView, d = f.getComputedStyle && "textfield" !== f.getComputedStyle(u, null).WebkitAppearance && 0 !== u.offsetHeight, q.removeChild(u)) : /^(search|tel)$/.test(e) || (d = /^(url|email)$/.test(e) ? u.checkValidity && u.checkValidity() === !1 : u.value != v)), D[a[g]] = !!d;
            return D
        }("search tel url email datetime date month week time datetime-local number range color".split(" "))
    }
    var l, m, n = "2.8.3",
      o = {},
      p = !0,
      q = b.documentElement,
      r = "modernizr",
      s = b.createElement(r),
      t = s.style,
      u = b.createElement("input"),
      v = ":)",
      w = {}.toString,
      x = " -webkit- -moz- -o- -ms- ".split(" "),
      y = "Webkit Moz O ms",
      z = y.split(" "),
      A = y.toLowerCase().split(" "),
      B = {
          svg: "http://www.w3.org/2000/svg"
      },
      C = {},
      D = {},
      E = {},
      F = [],
      G = F.slice,
      H = function (a, c, d, e) {
          var f, g, h, i, j = b.createElement("div"),
            k = b.body,
            l = k || b.createElement("body");
          if (parseInt(d, 10))
              for (; d--;) h = b.createElement("div"), h.id = e ? e[d] : r + (d + 1), j.appendChild(h);
          return f = ["&#173;", '<style id="s', r, '">', a, "</style>"].join(""), j.id = r, (k ? j : l).innerHTML += f, l.appendChild(j), k || (l.style.background = "", l.style.overflow = "hidden", i = q.style.overflow, q.style.overflow = "hidden", q.appendChild(l)), g = c(j, a), k ? j.parentNode.removeChild(j) : (l.parentNode.removeChild(l), q.style.overflow = i), !!g
      },
      I = function (b) {
          var c = a.matchMedia || a.msMatchMedia;
          if (c) return c(b) && c(b).matches || !1;
          var d;
          return H("@media " + b + " { #" + r + " { position: absolute; } }", function (b) {
              d = "absolute" == (a.getComputedStyle ? getComputedStyle(b, null) : b.currentStyle).position
          }), d
      },
      J = function () {
          function a(a, e) {
              e = e || b.createElement(d[a] || "div"), a = "on" + a;
              var g = a in e;
              return g || (e.setAttribute || (e = b.createElement("div")), e.setAttribute && e.removeAttribute && (e.setAttribute(a, ""), g = f(e[a], "function"), f(e[a], "undefined") || (e[a] = c), e.removeAttribute(a))), e = null, g
          }
          var d = {
              select: "input",
              change: "input",
              submit: "form",
              reset: "form",
              error: "img",
              load: "img",
              abort: "img"
          };
          return a
      }(),
      K = {}.hasOwnProperty;
    m = f(K, "undefined") || f(K.call, "undefined") ? function (a, b) {
        return b in a && f(a.constructor.prototype[b], "undefined")
    } : function (a, b) {
        return K.call(a, b)
    }, Function.prototype.bind || (Function.prototype.bind = function (a) {
        var b = this;
        if ("function" != typeof b) throw new TypeError;
        var c = G.call(arguments, 1),
          d = function () {
              if (this instanceof d) {
                  var e = function () { };
                  e.prototype = b.prototype;
                  var f = new e,
                    g = b.apply(f, c.concat(G.call(arguments)));
                  return Object(g) === g ? g : f
              }
              return b.apply(a, c.concat(G.call(arguments)))
          };
        return d
    }), C.flexbox = function () {
        return j("flexWrap")
    }, C.flexboxlegacy = function () {
        return j("boxDirection")
    }, C.canvas = function () {
        var a = b.createElement("canvas");
        return !(!a.getContext || !a.getContext("2d"))
    }, C.canvastext = function () {
        return !(!o.canvas || !f(b.createElement("canvas").getContext("2d").fillText, "function"))
    }, C.webgl = function () {
        return !!a.WebGLRenderingContext
    }, C.touch = function () {
        var c;
        return "ontouchstart" in a || a.DocumentTouch && b instanceof DocumentTouch ? c = !0 : H(["@media (", x.join("touch-enabled),("), r, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function (a) {
            c = 9 === a.offsetTop
        }), c
    }, C.geolocation = function () {
        return "geolocation" in navigator
    }, C.postmessage = function () {
        return !!a.postMessage
    }, C.websqldatabase = function () {
        return !!a.openDatabase
    }, C.indexedDB = function () {
        return !!j("indexedDB", a)
    }, C.hashchange = function () {
        return J("hashchange", a) && (b.documentMode === c || b.documentMode > 7)
    }, C.history = function () {
        return !(!a.history || !history.pushState)
    }, C.draganddrop = function () {
        var a = b.createElement("div");
        return "draggable" in a || "ondragstart" in a && "ondrop" in a
    }, C.websockets = function () {
        return "WebSocket" in a || "MozWebSocket" in a
    }, C.rgba = function () {
        return d("background-color:rgba(150,255,150,.5)"), g(t.backgroundColor, "rgba")
    }, C.hsla = function () {
        return d("background-color:hsla(120,40%,100%,.5)"), g(t.backgroundColor, "rgba") || g(t.backgroundColor, "hsla")
    }, C.multiplebgs = function () {
        return d("background:url(https://),url(https://),red url(https://)"), /(url\s*\(.*?){3}/.test(t.background)
    }, C.backgroundsize = function () {
        return j("backgroundSize")
    }, C.borderimage = function () {
        return j("borderImage")
    }, C.borderradius = function () {
        return j("borderRadius")
    }, C.boxshadow = function () {
        return j("boxShadow")
    }, C.textshadow = function () {
        return "" === b.createElement("div").style.textShadow
    }, C.opacity = function () {
        return e("opacity:.55"), /^0.55$/.test(t.opacity)
    }, C.cssanimations = function () {
        return j("animationName")
    }, C.csscolumns = function () {
        return j("columnCount")
    }, C.cssgradients = function () {
        var a = "background-image:",
          b = "gradient(linear,left top,right bottom,from(#9f9),to(white));",
          c = "linear-gradient(left top,#9f9, white);";
        return d((a + "-webkit- ".split(" ").join(b + a) + x.join(c + a)).slice(0, -a.length)), g(t.backgroundImage, "gradient")
    }, C.cssreflections = function () {
        return j("boxReflect")
    }, C.csstransforms = function () {
        return !!j("transform")
    }, C.csstransforms3d = function () {
        var a = !!j("perspective");
        return a && "webkitPerspective" in q.style && H("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function (b) {
            a = 9 === b.offsetLeft && 3 === b.offsetHeight
        }), a
    }, C.csstransitions = function () {
        return j("transition")
    }, C.fontface = function () {
        var a;
        return H('@font-face {font-family:"font";src:url("https://")}', function (c, d) {
            var e = b.getElementById("smodernizr"),
              f = e.sheet || e.styleSheet,
              g = f ? f.cssRules && f.cssRules[0] ? f.cssRules[0].cssText : f.cssText || "" : "";
            a = /src/i.test(g) && 0 === g.indexOf(d.split(" ")[0])
        }), a
    }, C.generatedcontent = function () {
        var a;
        return H(["#", r, "{font:0/0 a}#", r, ':after{content:"', v, '";visibility:hidden;font:3px/1 a}'].join(""), function (b) {
            a = b.offsetHeight >= 3
        }), a
    }, C.video = function () {
        var a = b.createElement("video"),
          c = !1;
        try {
            (c = !!a.canPlayType) && (c = new Boolean(c), c.ogg = a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""), c.h264 = a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), c.webm = a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, ""))
        } catch (d) { }
        return c
    }, C.audio = function () {
        var a = b.createElement("audio"),
          c = !1;
        try {
            (c = !!a.canPlayType) && (c = new Boolean(c), c.ogg = a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""), c.mp3 = a.canPlayType("audio/mpeg;").replace(/^no$/, ""), c.wav = a.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""), c.m4a = (a.canPlayType("audio/x-m4a;") || a.canPlayType("audio/aac;")).replace(/^no$/, ""))
        } catch (d) { }
        return c
    }, C.localstorage = function () {
        try {
            return localStorage.setItem(r, r), localStorage.removeItem(r), !0
        } catch (a) {
            return !1
        }
    }, C.sessionstorage = function () {
        try {
            return sessionStorage.setItem(r, r), sessionStorage.removeItem(r), !0
        } catch (a) {
            return !1
        }
    }, C.webworkers = function () {
        return !!a.Worker
    }, C.applicationcache = function () {
        return !!a.applicationCache
    }, C.svg = function () {
        return !!b.createElementNS && !!b.createElementNS(B.svg, "svg").createSVGRect
    }, C.inlinesvg = function () {
        var a = b.createElement("div");
        return a.innerHTML = "<svg/>", (a.firstChild && a.firstChild.namespaceURI) == B.svg
    }, C.smil = function () {
        return !!b.createElementNS && /SVGAnimate/.test(w.call(b.createElementNS(B.svg, "animate")))
    }, C.svgclippaths = function () {
        return !!b.createElementNS && /SVGClipPath/.test(w.call(b.createElementNS(B.svg, "clipPath")))
    };
    for (var L in C) m(C, L) && (l = L.toLowerCase(), o[l] = C[L](), F.push((o[l] ? "" : "no-") + l));
    return o.input || k(), o.addTest = function (a, b) {
        if ("object" == typeof a)
            for (var d in a) m(a, d) && o.addTest(d, a[d]);
        else {
            if (a = a.toLowerCase(), o[a] !== c) return o;
            b = "function" == typeof b ? b() : b, "undefined" != typeof p && p && (q.className += " " + (b ? "" : "no-") + a), o[a] = b
        }
        return o
    }, d(""), s = u = null,
      function (a, b) {
          function c(a, b) {
              var c = a.createElement("p"),
                d = a.getElementsByTagName("head")[0] || a.documentElement;
              return c.innerHTML = "x<style>" + b + "</style>", d.insertBefore(c.lastChild, d.firstChild)
          }

          function d() {
              var a = s.elements;
              return "string" == typeof a ? a.split(" ") : a
          }

          function e(a) {
              var b = r[a[p]];
              return b || (b = {}, q++, a[p] = q, r[q] = b), b
          }

          function f(a, c, d) {
              if (c || (c = b), k) return c.createElement(a);
              d || (d = e(c));
              var f;
              return f = d.cache[a] ? d.cache[a].cloneNode() : o.test(a) ? (d.cache[a] = d.createElem(a)).cloneNode() : d.createElem(a), !f.canHaveChildren || n.test(a) || f.tagUrn ? f : d.frag.appendChild(f)
          }

          function g(a, c) {
              if (a || (a = b), k) return a.createDocumentFragment();
              c = c || e(a);
              for (var f = c.frag.cloneNode(), g = 0, h = d(), i = h.length; i > g; g++) f.createElement(h[g]);
              return f
          }

          function h(a, b) {
              b.cache || (b.cache = {}, b.createElem = a.createElement, b.createFrag = a.createDocumentFragment, b.frag = b.createFrag()), a.createElement = function (c) {
                  return s.shivMethods ? f(c, a, b) : b.createElem(c)
              }, a.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + d().join().replace(/[\w\-]+/g, function (a) {
                  return b.createElem(a), b.frag.createElement(a), 'c("' + a + '")'
              }) + ");return n}")(s, b.frag)
          }

          function i(a) {
              a || (a = b);
              var d = e(a);
              return !s.shivCSS || j || d.hasCSS || (d.hasCSS = !!c(a, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), k || h(a, d), a
          }
          var j, k, l = "3.7.0",
            m = a.html5 || {},
            n = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
            o = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
            p = "_html5shiv",
            q = 0,
            r = {};
          ! function () {
              try {
                  var a = b.createElement("a");
                  a.innerHTML = "<xyz></xyz>", j = "hidden" in a, k = 1 == a.childNodes.length || function () {
                      b.createElement("a");
                      var a = b.createDocumentFragment();
                      return "undefined" == typeof a.cloneNode || "undefined" == typeof a.createDocumentFragment || "undefined" == typeof a.createElement
                  }()
              } catch (c) {
                  j = !0, k = !0
              }
          }();
          var s = {
              elements: m.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
              version: l,
              shivCSS: m.shivCSS !== !1,
              supportsUnknownElements: k,
              shivMethods: m.shivMethods !== !1,
              type: "default",
              shivDocument: i,
              createElement: f,
              createDocumentFragment: g
          };
          a.html5 = s, i(b)
      }(this, b), o._version = n, o._prefixes = x, o._domPrefixes = A, o._cssomPrefixes = z, o.mq = I, o.hasEvent = J, o.testProp = function (a) {
          return h([a])
      }, o.testAllProps = j, o.testStyles = H, o.prefixed = function (a, b, c) {
          return b ? j(a, b, c) : j(a, "pfx")
      }, q.className = q.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (p ? " js " + F.join(" ") : ""), o
}(this, this.document),
  function () {
      "use strict";

      function a() { }

      function b(a, b) {
          for (var c = a.length; c--;)
              if (a[c].listener === b) return c;
          return -1
      }

      function c(a) {
          return function () {
              return this[a].apply(this, arguments)
          }
      }
      var d = a.prototype,
        e = this,
        f = e.EventEmitter;
      d.getListeners = function (a) {
          var b, c, d = this._getEvents();
          if (a instanceof RegExp) {
              b = {};
              for (c in d) d.hasOwnProperty(c) && a.test(c) && (b[c] = d[c])
          } else b = d[a] || (d[a] = []);
          return b
      }, d.flattenListeners = function (a) {
          var b, c = [];
          for (b = 0; b < a.length; b += 1) c.push(a[b].listener);
          return c
      }, d.getListenersAsObject = function (a) {
          var b, c = this.getListeners(a);
          return c instanceof Array && (b = {}, b[a] = c), b || c
      }, d.addListener = function (a, c) {
          var d, e = this.getListenersAsObject(a),
            f = "object" == typeof c;
          for (d in e) e.hasOwnProperty(d) && -1 === b(e[d], c) && e[d].push(f ? c : {
              listener: c,
              once: !1
          });
          return this
      }, d.on = c("addListener"), d.addOnceListener = function (a, b) {
          return this.addListener(a, {
              listener: b,
              once: !0
          })
      }, d.once = c("addOnceListener"), d.defineEvent = function (a) {
          return this.getListeners(a), this
      }, d.defineEvents = function (a) {
          for (var b = 0; b < a.length; b += 1) this.defineEvent(a[b]);
          return this
      }, d.removeListener = function (a, c) {
          var d, e, f = this.getListenersAsObject(a);
          for (e in f) f.hasOwnProperty(e) && (d = b(f[e], c), -1 !== d && f[e].splice(d, 1));
          return this
      }, d.off = c("removeListener"), d.addListeners = function (a, b) {
          return this.manipulateListeners(!1, a, b)
      }, d.removeListeners = function (a, b) {
          return this.manipulateListeners(!0, a, b)
      }, d.manipulateListeners = function (a, b, c) {
          var d, e, f = a ? this.removeListener : this.addListener,
            g = a ? this.removeListeners : this.addListeners;
          if ("object" != typeof b || b instanceof RegExp)
              for (d = c.length; d--;) f.call(this, b, c[d]);
          else
              for (d in b) b.hasOwnProperty(d) && (e = b[d]) && ("function" == typeof e ? f.call(this, d, e) : g.call(this, d, e));
          return this
      }, d.removeEvent = function (a) {
          var b, c = typeof a,
            d = this._getEvents();
          if ("string" === c) delete d[a];
          else if (a instanceof RegExp)
              for (b in d) d.hasOwnProperty(b) && a.test(b) && delete d[b];
          else delete this._events;
          return this
      }, d.removeAllListeners = c("removeEvent"), d.emitEvent = function (a, b) {
          var c, d, e, f, g, h = this.getListenersAsObject(a);
          for (f in h)
              if (h.hasOwnProperty(f))
                  for (c = h[f].slice(0), e = c.length; e--;) d = c[e], d.once === !0 && this.removeListener(a, d.listener), g = d.listener.apply(this, b || []), g === this._getOnceReturnValue() && this.removeListener(a, d.listener);
          return this
      }, d.trigger = c("emitEvent"), d.emit = function (a) {
          var b = Array.prototype.slice.call(arguments, 1);
          return this.emitEvent(a, b)
      }, d.setOnceReturnValue = function (a) {
          return this._onceReturnValue = a, this
      }, d._getOnceReturnValue = function () {
          return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
      }, d._getEvents = function () {
          return this._events || (this._events = {})
      }, a.noConflict = function () {
          return e.EventEmitter = f, a
      }, "function" == typeof define && define.amd ? define(function () {
          return a
      }) : "object" == typeof module && module.exports ? module.exports = a : e.EventEmitter = a
  }.call(this),
  function (a) {
      "use strict";

      function b(b) {
          var c = a.event;
          return c.target = c.target || c.srcElement || b, c
      }
      var c = document.documentElement,
        d = function () { };
      c.addEventListener ? d = function (a, b, c) {
          a.addEventListener(b, c, !1)
      } : c.attachEvent && (d = function (a, c, d) {
          a[c + d] = d.handleEvent ? function () {
              var c = b(a);
              d.handleEvent.call(d, c)
          } : function () {
              var c = b(a);
              d.call(a, c)
          }, a.attachEvent("on" + c, a[c + d])
      });
      var e = function () { };
      c.removeEventListener ? e = function (a, b, c) {
          a.removeEventListener(b, c, !1)
      } : c.detachEvent && (e = function (a, b, c) {
          a.detachEvent("on" + b, a[b + c]);
          try {
              delete a[b + c]
          } catch (d) {
              a[b + c] = void 0
          }
      });
      var f = {
          bind: d,
          unbind: e
      };
      "function" == typeof define && define.amd ? define(f) : "object" == typeof exports ? module.exports = f : a.eventie = f
  }(window),
  function (a, b) {
      "use strict";
      "function" == typeof define && define.amd ? define(["eventEmitter/EventEmitter", "eventie/eventie"], function (c, d) {
          return b(a, c, d)
      }) : "object" == typeof module && module.exports ? module.exports = b(a, require("wolfy87-eventemitter"), require("eventie")) : a.imagesLoaded = b(a, a.EventEmitter, a.eventie)
  }(window, function (a, b, c) {
      "use strict";

      function d(a, b) {
          for (var c in b) a[c] = b[c];
          return a
      }

      function e(a) {
          return "[object Array]" == l.call(a)
      }

      function f(a) {
          var b = [];
          if (e(a)) b = a;
          else if ("number" == typeof a.length)
              for (var c = 0; c < a.length; c++) b.push(a[c]);
          else b.push(a);
          return b
      }

      function g(a, b, c) {
          if (!(this instanceof g)) return new g(a, b, c);
          "string" == typeof a && (a = document.querySelectorAll(a)), this.elements = f(a), this.options = d({}, this.options), "function" == typeof b ? c = b : d(this.options, b), c && this.on("always", c), this.getImages(), j && (this.jqDeferred = new j.Deferred);
          var e = this;
          setTimeout(function () {
              e.check()
          })
      }

      function h(a) {
          this.img = a
      }

      function i(a, b) {
          this.url = a, this.element = b, this.img = new Image
      }
      var j = a.jQuery,
        k = a.console,
        l = Object.prototype.toString;
      g.prototype = new b, g.prototype.options = {}, g.prototype.getImages = function () {
          this.images = [];
          for (var a = 0; a < this.elements.length; a++) {
              var b = this.elements[a];
              this.addElementImages(b)
          }
      }, g.prototype.addElementImages = function (a) {
          "IMG" == a.nodeName && this.addImage(a), this.options.background === !0 && this.addElementBackgroundImages(a);
          var b = a.nodeType;
          if (b && m[b]) {
              for (var c = a.querySelectorAll("img"), d = 0; d < c.length; d++) {
                  var e = c[d];
                  this.addImage(e)
              }
              if ("string" == typeof this.options.background) {
                  var f = a.querySelectorAll(this.options.background);
                  for (d = 0; d < f.length; d++) {
                      var g = f[d];
                      this.addElementBackgroundImages(g)
                  }
              }
          }
      };
      var m = {
          1: !0,
          9: !0,
          11: !0
      };
      g.prototype.addElementBackgroundImages = function (a) {
          for (var b = n(a), c = /url\(['"]*([^'"\)]+)['"]*\)/gi, d = c.exec(b.backgroundImage) ; null !== d;) {
              var e = d && d[1];
              e && this.addBackground(e, a), d = c.exec(b.backgroundImage)
          }
      };
      var n = a.getComputedStyle || function (a) {
          return a.currentStyle
      };
      return g.prototype.addImage = function (a) {
          var b = new h(a);
          this.images.push(b)
      }, g.prototype.addBackground = function (a, b) {
          var c = new i(a, b);
          this.images.push(c)
      }, g.prototype.check = function () {
          function a(a, c, d) {
              setTimeout(function () {
                  b.progress(a, c, d)
              })
          }
          var b = this;
          if (this.progressedCount = 0, this.hasAnyBroken = !1, !this.images.length) return void this.complete();
          for (var c = 0; c < this.images.length; c++) {
              var d = this.images[c];
              d.once("progress", a), d.check()
          }
      }, g.prototype.progress = function (a, b, c) {
          this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !a.isLoaded, this.emit("progress", this, a, b), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, a), this.progressedCount == this.images.length && this.complete(), this.options.debug && k && k.log("progress: " + c, a, b)
      }, g.prototype.complete = function () {
          var a = this.hasAnyBroken ? "fail" : "done";
          if (this.isComplete = !0, this.emit(a, this), this.emit("always", this), this.jqDeferred) {
              var b = this.hasAnyBroken ? "reject" : "resolve";
              this.jqDeferred[b](this)
          }
      }, h.prototype = new b, h.prototype.check = function () {
          var a = this.getIsImageComplete();
          return a ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image, c.bind(this.proxyImage, "load", this), c.bind(this.proxyImage, "error", this), c.bind(this.img, "load", this), c.bind(this.img, "error", this), void (this.proxyImage.src = this.img.src))
      }, h.prototype.getIsImageComplete = function () {
          return this.img.complete && void 0 !== this.img.naturalWidth
      }, h.prototype.confirm = function (a, b) {
          this.isLoaded = a, this.emit("progress", this, this.img, b)
      }, h.prototype.handleEvent = function (a) {
          var b = "on" + a.type;
          this[b] && this[b](a)
      }, h.prototype.onload = function () {
          this.confirm(!0, "onload"), this.unbindEvents()
      }, h.prototype.onerror = function () {
          this.confirm(!1, "onerror"), this.unbindEvents()
      }, h.prototype.unbindEvents = function () {
          c.unbind(this.proxyImage, "load", this), c.unbind(this.proxyImage, "error", this), c.unbind(this.img, "load", this), c.unbind(this.img, "error", this)
      }, i.prototype = new h, i.prototype.check = function () {
          c.bind(this.img, "load", this), c.bind(this.img, "error", this), this.img.src = this.url;
          var a = this.getIsImageComplete();
          a && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents())
      }, i.prototype.unbindEvents = function () {
          c.unbind(this.img, "load", this), c.unbind(this.img, "error", this)
      }, i.prototype.confirm = function (a, b) {
          this.isLoaded = a, this.emit("progress", this, this.element, b)
      }, g.makeJQueryPlugin = function (b) {
          b = b || a.jQuery, b && (j = b, j.fn.imagesLoaded = function (a, b) {
              var c = new g(this, a, b);
              return c.jqDeferred.promise(j(this))
          })
      }, g.makeJQueryPlugin(), g
  }),
  function (a) {
      "use strict";

      function b(a) {
          return (a || "").toLowerCase()
      }
      var c = "2.1.6";
      a.fn.cycle = function (c) {
          var d;
          return 0 !== this.length || a.isReady ? this.each(function () {
              var d, e, f, g, h = a(this),
                i = a.fn.cycle.log;
              if (!h.data("cycle.opts")) {
                  (h.data("cycle-log") === !1 || c && c.log === !1 || e && e.log === !1) && (i = a.noop), i("--c2 init--"), d = h.data();
                  for (var j in d) d.hasOwnProperty(j) && /^cycle[A-Z]+/.test(j) && (g = d[j], f = j.match(/^cycle(.*)/)[1].replace(/^[A-Z]/, b), i(f + ":", g, "(" + typeof g + ")"), d[f] = g);
                  e = a.extend({}, a.fn.cycle.defaults, d, c || {}), e.timeoutId = 0, e.paused = e.paused || !1, e.container = h, e._maxZ = e.maxZ, e.API = a.extend({
                      _container: h
                  }, a.fn.cycle.API), e.API.log = i, e.API.trigger = function (a, b) {
                      return e.container.trigger(a, b), e.API
                  }, h.data("cycle.opts", e), h.data("cycle.API", e.API), e.API.trigger("cycle-bootstrap", [e, e.API]), e.API.addInitialSlides(), e.API.preInitSlideshow(), e.slides.length && e.API.initSlideshow()
              }
          }) : (d = {
              s: this.selector,
              c: this.context
          }, a.fn.cycle.log("requeuing slideshow (dom not ready)"), a(function () {
              a(d.s, d.c).cycle(c)
          }), this)
      }, a.fn.cycle.API = {
          opts: function () {
              return this._container.data("cycle.opts")
          },
          addInitialSlides: function () {
              var b = this.opts(),
                c = b.slides;
              b.slideCount = 0, b.slides = a(), c = c.jquery ? c : b.container.find(c), b.random && c.sort(function () {
                  return Math.random() - .5
              }), b.API.add(c)
          },
          preInitSlideshow: function () {
              var b = this.opts();
              b.API.trigger("cycle-pre-initialize", [b]);
              var c = a.fn.cycle.transitions[b.fx];
              c && a.isFunction(c.preInit) && c.preInit(b), b._preInitialized = !0
          },
          postInitSlideshow: function () {
              var b = this.opts();
              b.API.trigger("cycle-post-initialize", [b]);
              var c = a.fn.cycle.transitions[b.fx];
              c && a.isFunction(c.postInit) && c.postInit(b)
          },
          initSlideshow: function () {
              var b, c = this.opts(),
                d = c.container;
              c.API.calcFirstSlide(), "static" == c.container.css("position") && c.container.css("position", "relative"), a(c.slides[c.currSlide]).css({
                  opacity: 1,
                  display: "block",
                  visibility: "visible"
              }), c.API.stackSlides(c.slides[c.currSlide], c.slides[c.nextSlide], !c.reverse), c.pauseOnHover && (c.pauseOnHover !== !0 && (d = a(c.pauseOnHover)), d.hover(function () {
                  c.API.pause(!0)
              }, function () {
                  c.API.resume(!0)
              })), c.timeout && (b = c.API.getSlideOpts(c.currSlide), c.API.queueTransition(b, b.timeout + c.delay)), c._initialized = !0, c.API.updateView(!0), c.API.trigger("cycle-initialized", [c]), c.API.postInitSlideshow()
          },
          pause: function (b) {
              var c = this.opts(),
                d = c.API.getSlideOpts(),
                e = c.hoverPaused || c.paused;
              b ? c.hoverPaused = !0 : c.paused = !0, e || (c.container.addClass("cycle-paused"), c.API.trigger("cycle-paused", [c]).log("cycle-paused"), d.timeout && (clearTimeout(c.timeoutId), c.timeoutId = 0, c._remainingTimeout -= a.now() - c._lastQueue, (c._remainingTimeout < 0 || isNaN(c._remainingTimeout)) && (c._remainingTimeout = void 0)))
          },
          resume: function (a) {
              var b = this.opts(),
                c = !b.hoverPaused && !b.paused;
              a ? b.hoverPaused = !1 : b.paused = !1, c || (b.container.removeClass("cycle-paused"), 0 === b.slides.filter(":animated").length && b.API.queueTransition(b.API.getSlideOpts(), b._remainingTimeout), b.API.trigger("cycle-resumed", [b, b._remainingTimeout]).log("cycle-resumed"))
          },
          add: function (b, c) {
              var d, e = this.opts(),
                f = e.slideCount,
                g = !1;
              "string" == a.type(b) && (b = a.trim(b)), a(b).each(function () {
                  var b, d = a(this);
                  c ? e.container.prepend(d) : e.container.append(d), e.slideCount++, b = e.API.buildSlideOpts(d), e.slides = c ? a(d).add(e.slides) : e.slides.add(d), e.API.initSlide(b, d, --e._maxZ), d.data("cycle.opts", b), e.API.trigger("cycle-slide-added", [e, b, d])
              }), e.API.updateView(!0), g = e._preInitialized && 2 > f && e.slideCount >= 1, g && (e._initialized ? e.timeout && (d = e.slides.length, e.nextSlide = e.reverse ? d - 1 : 1, e.timeoutId || e.API.queueTransition(e)) : e.API.initSlideshow())
          },
          calcFirstSlide: function () {
              var a, b = this.opts();
              a = parseInt(b.startingSlide || 0, 10), (a >= b.slides.length || 0 > a) && (a = 0), b.currSlide = a, b.reverse ? (b.nextSlide = a - 1, b.nextSlide < 0 && (b.nextSlide = b.slides.length - 1)) : (b.nextSlide = a + 1, b.nextSlide == b.slides.length && (b.nextSlide = 0))
          },
          calcNextSlide: function () {
              var a, b = this.opts();
              b.reverse ? (a = b.nextSlide - 1 < 0, b.nextSlide = a ? b.slideCount - 1 : b.nextSlide - 1, b.currSlide = a ? 0 : b.nextSlide + 1) : (a = b.nextSlide + 1 == b.slides.length, b.nextSlide = a ? 0 : b.nextSlide + 1, b.currSlide = a ? b.slides.length - 1 : b.nextSlide - 1)
          },
          calcTx: function (b, c) {
              var d, e = b;
              return e._tempFx ? d = a.fn.cycle.transitions[e._tempFx] : c && e.manualFx && (d = a.fn.cycle.transitions[e.manualFx]), d || (d = a.fn.cycle.transitions[e.fx]), e._tempFx = null, this.opts()._tempFx = null, d || (d = a.fn.cycle.transitions.fade, e.API.log('Transition "' + e.fx + '" not found.  Using fade.')), d
          },
          prepareTx: function (a, b) {
              var c, d, e, f, g, h = this.opts();
              return h.slideCount < 2 ? void (h.timeoutId = 0) : (!a || h.busy && !h.manualTrump || (h.API.stopTransition(), h.busy = !1, clearTimeout(h.timeoutId), h.timeoutId = 0), void (h.busy || (0 !== h.timeoutId || a) && (d = h.slides[h.currSlide], e = h.slides[h.nextSlide], f = h.API.getSlideOpts(h.nextSlide), g = h.API.calcTx(f, a), h._tx = g, a && void 0 !== f.manualSpeed && (f.speed = f.manualSpeed), h.nextSlide != h.currSlide && (a || !h.paused && !h.hoverPaused && h.timeout) ? (h.API.trigger("cycle-before", [f, d, e, b]), g.before && g.before(f, d, e, b), c = function () {
                  h.busy = !1, h.container.data("cycle.opts") && (g.after && g.after(f, d, e, b), h.API.trigger("cycle-after", [f, d, e, b]), h.API.queueTransition(f), h.API.updateView(!0))
              }, h.busy = !0, g.transition ? g.transition(f, d, e, b, c) : h.API.doTransition(f, d, e, b, c), h.API.calcNextSlide(), h.API.updateView()) : h.API.queueTransition(f))))
          },
          doTransition: function (b, c, d, e, f) {
              var g = b,
                h = a(c),
                i = a(d),
                j = function () {
                    i.animate(g.animIn || {
                        opacity: 1
                    }, g.speed, g.easeIn || g.easing, f)
                };
              i.css(g.cssBefore || {}), h.animate(g.animOut || {}, g.speed, g.easeOut || g.easing, function () {
                  h.css(g.cssAfter || {}), g.sync || j()
              }), g.sync && j()
          },
          queueTransition: function (b, c) {
              var d = this.opts(),
                e = void 0 !== c ? c : b.timeout;
              return 0 === d.nextSlide && 0 === --d.loop ? (d.API.log("terminating; loop=0"), d.timeout = 0, e ? setTimeout(function () {
                  d.API.trigger("cycle-finished", [d])
              }, e) : d.API.trigger("cycle-finished", [d]), void (d.nextSlide = d.currSlide)) : void 0 !== d.continueAuto && (d.continueAuto === !1 || a.isFunction(d.continueAuto) && d.continueAuto() === !1) ? (d.API.log("terminating automatic transitions"), d.timeout = 0, void (d.timeoutId && clearTimeout(d.timeoutId))) : void (e && (d._lastQueue = a.now(), void 0 === c && (d._remainingTimeout = b.timeout), d.paused || d.hoverPaused || (d.timeoutId = setTimeout(function () {
                  d.API.prepareTx(!1, !d.reverse)
              }, e))))
          },
          stopTransition: function () {
              var a = this.opts();
              a.slides.filter(":animated").length && (a.slides.stop(!1, !0), a.API.trigger("cycle-transition-stopped", [a])), a._tx && a._tx.stopTransition && a._tx.stopTransition(a)
          },
          advanceSlide: function (a) {
              var b = this.opts();
              return clearTimeout(b.timeoutId), b.timeoutId = 0, b.nextSlide = b.currSlide + a, b.nextSlide < 0 ? b.nextSlide = b.slides.length - 1 : b.nextSlide >= b.slides.length && (b.nextSlide = 0), b.API.prepareTx(!0, a >= 0), !1
          },
          buildSlideOpts: function (c) {
              var d, e, f = this.opts(),
                g = c.data() || {};
              for (var h in g) g.hasOwnProperty(h) && /^cycle[A-Z]+/.test(h) && (d = g[h], e = h.match(/^cycle(.*)/)[1].replace(/^[A-Z]/, b), f.API.log("[" + (f.slideCount - 1) + "]", e + ":", d, "(" + typeof d + ")"), g[e] = d);
              g = a.extend({}, a.fn.cycle.defaults, f, g), g.slideNum = f.slideCount;
              try {
                  delete g.API, delete g.slideCount, delete g.currSlide, delete g.nextSlide, delete g.slides
              } catch (i) { }
              return g
          },
          getSlideOpts: function (b) {
              var c = this.opts();
              void 0 === b && (b = c.currSlide);
              var d = c.slides[b],
                e = a(d).data("cycle.opts");
              return a.extend({}, c, e)
          },
          initSlide: function (b, c, d) {
              var e = this.opts();
              c.css(b.slideCss || {}), d > 0 && c.css("zIndex", d), isNaN(b.speed) && (b.speed = a.fx.speeds[b.speed] || a.fx.speeds._default), b.sync || (b.speed = b.speed / 2), c.addClass(e.slideClass)
          },
          updateView: function (a, b) {
              var c = this.opts();
              if (c._initialized) {
                  var d = c.API.getSlideOpts(),
                    e = c.slides[c.currSlide];
                  !a && b !== !0 && (c.API.trigger("cycle-update-view-before", [c, d, e]), c.updateView < 0) || (c.slideActiveClass && c.slides.removeClass(c.slideActiveClass).eq(c.currSlide).addClass(c.slideActiveClass), a && c.hideNonActive && c.slides.filter(":not(." + c.slideActiveClass + ")").css("visibility", "hidden"), 0 === c.updateView && setTimeout(function () {
                      c.API.trigger("cycle-update-view", [c, d, e, a])
                  }, d.speed / (c.sync ? 2 : 1)), 0 !== c.updateView && c.API.trigger("cycle-update-view", [c, d, e, a]), a && c.API.trigger("cycle-update-view-after", [c, d, e]))
              }
          },
          getComponent: function (b) {
              var c = this.opts(),
                d = c[b];
              return "string" == typeof d ? /^\s*[\>|\+|~]/.test(d) ? c.container.find(d) : a(d) : d.jquery ? d : a(d)
          },
          stackSlides: function (b, c, d) {
              var e = this.opts();
              b || (b = e.slides[e.currSlide], c = e.slides[e.nextSlide], d = !e.reverse), a(b).css("zIndex", e.maxZ);
              var f, g = e.maxZ - 2,
                h = e.slideCount;
              if (d) {
                  for (f = e.currSlide + 1; h > f; f++) a(e.slides[f]).css("zIndex", g--);
                  for (f = 0; f < e.currSlide; f++) a(e.slides[f]).css("zIndex", g--)
              } else {
                  for (f = e.currSlide - 1; f >= 0; f--) a(e.slides[f]).css("zIndex", g--);
                  for (f = h - 1; f > e.currSlide; f--) a(e.slides[f]).css("zIndex", g--)
              }
              a(c).css("zIndex", e.maxZ - 1)
          },
          getSlideIndex: function (a) {
              return this.opts().slides.index(a)
          }
      }, a.fn.cycle.log = function () {
          window.console && console.log && console.log("[cycle2] " + Array.prototype.join.call(arguments, " "))
      }, a.fn.cycle.version = function () {
          return "Cycle2: " + c
      }, a.fn.cycle.transitions = {
          custom: {},
          none: {
              before: function (a, b, c, d) {
                  a.API.stackSlides(c, b, d), a.cssBefore = {
                      opacity: 1,
                      visibility: "visible",
                      display: "block"
                  }
              }
          },
          fade: {
              before: function (b, c, d, e) {
                  var f = b.API.getSlideOpts(b.nextSlide).slideCss || {};
                  b.API.stackSlides(c, d, e), b.cssBefore = a.extend(f, {
                      opacity: 0,
                      visibility: "visible",
                      display: "block"
                  }), b.animIn = {
                      opacity: 1
                  }, b.animOut = {
                      opacity: 0
                  }
              }
          },
          fadeout: {
              before: function (b, c, d, e) {
                  var f = b.API.getSlideOpts(b.nextSlide).slideCss || {};
                  b.API.stackSlides(c, d, e), b.cssBefore = a.extend(f, {
                      opacity: 1,
                      visibility: "visible",
                      display: "block"
                  }), b.animOut = {
                      opacity: 0
                  }
              }
          },
          scrollHorz: {
              before: function (a, b, c, d) {
                  a.API.stackSlides(b, c, d);
                  var e = a.container.css("overflow", "hidden").width();
                  a.cssBefore = {
                      left: d ? e : -e,
                      top: 0,
                      opacity: 1,
                      visibility: "visible",
                      display: "block"
                  }, a.cssAfter = {
                      zIndex: a._maxZ - 2,
                      left: 0
                  }, a.animIn = {
                      left: 0
                  }, a.animOut = {
                      left: d ? -e : e
                  }
              }
          }
      }, a.fn.cycle.defaults = {
          allowWrap: !0,
          autoSelector: ".cycle-slideshow[data-cycle-auto-init!=false]",
          delay: 0,
          easing: null,
          fx: "fade",
          hideNonActive: !0,
          loop: 0,
          manualFx: void 0,
          manualSpeed: void 0,
          manualTrump: !0,
          maxZ: 100,
          pauseOnHover: !1,
          reverse: !1,
          slideActiveClass: "cycle-slide-active",
          slideClass: "cycle-slide",
          slideCss: {
              position: "absolute",
              top: 0,
              left: 0
          },
          slides: "> img",
          speed: 500,
          startingSlide: 0,
          sync: !0,
          timeout: 4e3,
          updateView: 0
      }, a(document).ready(function () {
          a(a.fn.cycle.defaults.autoSelector).cycle()
      })
  }(jQuery),
  function (a) {
      "use strict";

      function b(b, d) {
          var e, f, g, h = d.autoHeight;
          if ("container" == h) f = a(d.slides[d.currSlide]).outerHeight(), d.container.height(f);
          else if (d._autoHeightRatio) d.container.height(d.container.width() / d._autoHeightRatio);
          else if ("calc" === h || "number" == a.type(h) && h >= 0) {
              if (g = "calc" === h ? c(b, d) : h >= d.slides.length ? 0 : h, g == d._sentinelIndex) return;
              d._sentinelIndex = g, d._sentinel && d._sentinel.remove(), e = a(d.slides[g].cloneNode(!0)), e.removeAttr("id name rel").find("[id],[name],[rel]").removeAttr("id name rel"), e.css({
                  position: "static",
                  visibility: "hidden",
                  display: "block"
              }).prependTo(d.container).addClass("cycle-sentinel cycle-slide").removeClass("cycle-slide-active"), e.find("*").css("visibility", "hidden"), d._sentinel = e
          }
      }

      function c(b, c) {
          var d = 0,
            e = -1;
          return c.slides.each(function (b) {
              var c = a(this).height();
              c > e && (e = c, d = b)
          }), d
      }

      function d(b, c, d, e) {
          var f = a(e).outerHeight();
          c.container.animate({
              height: f
          }, c.autoHeightSpeed, c.autoHeightEasing)
      }

      function e(c, f) {
          f._autoHeightOnResize && (a(window).off("resize orientationchange", f._autoHeightOnResize), f._autoHeightOnResize = null), f.container.off("cycle-slide-added cycle-slide-removed", b), f.container.off("cycle-destroyed", e), f.container.off("cycle-before", d), f._sentinel && (f._sentinel.remove(), f._sentinel = null)
      }
      a.extend(a.fn.cycle.defaults, {
          autoHeight: 0,
          autoHeightSpeed: 250,
          autoHeightEasing: null
      }), a(document).on("cycle-initialized", function (c, f) {
          function g() {
              b(c, f)
          }
          var h, i = f.autoHeight,
            j = a.type(i),
            k = null;
          ("string" === j || "number" === j) && (f.container.on("cycle-slide-added cycle-slide-removed", b), f.container.on("cycle-destroyed", e), "container" == i ? f.container.on("cycle-before", d) : "string" === j && /\d+\:\d+/.test(i) && (h = i.match(/(\d+)\:(\d+)/), h = h[1] / h[2], f._autoHeightRatio = h), "number" !== j && (f._autoHeightOnResize = function () {
              clearTimeout(k), k = setTimeout(g, 50)
          }, a(window).on("resize orientationchange", f._autoHeightOnResize)), setTimeout(g, 30))
      })
  }(jQuery),
  function (a) {
      "use strict";
      a.extend(a.fn.cycle.defaults, {
          caption: "> .cycle-caption",
          captionTemplate: "{{slideNum}} / {{slideCount}}",
          overlay: "> .cycle-overlay",
          overlayTemplate: "<div>{{title}}</div><div>{{desc}}</div>",
          captionModule: "caption"
      }), a(document).on("cycle-update-view", function (b, c, d, e) {
          if ("caption" === c.captionModule) {
              a.each(["caption", "overlay"], function () {
                  var a = this,
                    b = d[a + "Template"],
                    f = c.API.getComponent(a);
                  f.length && b ? (f.html(c.API.tmpl(b, d, c, e)), f.show()) : f.hide()
              })
          }
      }), a(document).on("cycle-destroyed", function (b, c) {
          var d;
          a.each(["caption", "overlay"], function () {
              var a = this,
                b = c[a + "Template"];
              c[a] && b && (d = c.API.getComponent("caption"), d.empty())
          })
      })
  }(jQuery),
  function (a) {
      "use strict";
      var b = a.fn.cycle;
      a.fn.cycle = function (c) {
          var d, e, f, g = a.makeArray(arguments);
          return "number" == a.type(c) ? this.cycle("goto", c) : "string" == a.type(c) ? this.each(function () {
              var h;
              return d = c, f = a(this).data("cycle.opts"), void 0 === f ? void b.log('slideshow must be initialized before sending commands; "' + d + '" ignored') : (d = "goto" == d ? "jump" : d, e = f.API[d], a.isFunction(e) ? (h = a.makeArray(g), h.shift(), e.apply(f.API, h)) : void b.log("unknown command: ", d))
          }) : b.apply(this, arguments)
      }, a.extend(a.fn.cycle, b), a.extend(b.API, {
          next: function () {
              var a = this.opts();
              if (!a.busy || a.manualTrump) {
                  var b = a.reverse ? -1 : 1;
                  a.allowWrap === !1 && a.currSlide + b >= a.slideCount || (a.API.advanceSlide(b), a.API.trigger("cycle-next", [a]).log("cycle-next"))
              }
          },
          prev: function () {
              var a = this.opts();
              if (!a.busy || a.manualTrump) {
                  var b = a.reverse ? 1 : -1;
                  a.allowWrap === !1 && a.currSlide + b < 0 || (a.API.advanceSlide(b), a.API.trigger("cycle-prev", [a]).log("cycle-prev"))
              }
          },
          destroy: function () {
              this.stop();
              var b = this.opts(),
                c = a.isFunction(a._data) ? a._data : a.noop;
              clearTimeout(b.timeoutId), b.timeoutId = 0, b.API.stop(), b.API.trigger("cycle-destroyed", [b]).log("cycle-destroyed"), b.container.removeData(), c(b.container[0], "parsedAttrs", !1), b.retainStylesOnDestroy || (b.container.removeAttr("style"), b.slides.removeAttr("style"), b.slides.removeClass(b.slideActiveClass)), b.slides.each(function () {
                  var d = a(this);
                  d.removeData(), d.removeClass(b.slideClass), c(this, "parsedAttrs", !1)
              })
          },
          jump: function (a, b) {
              var c, d = this.opts();
              if (!d.busy || d.manualTrump) {
                  var e = parseInt(a, 10);
                  if (isNaN(e) || 0 > e || e >= d.slides.length) return void d.API.log("goto: invalid slide index: " + e);
                  if (e == d.currSlide) return void d.API.log("goto: skipping, already on slide", e);
                  d.nextSlide = e, clearTimeout(d.timeoutId), d.timeoutId = 0, d.API.log("goto: ", e, " (zero-index)"), c = d.currSlide < d.nextSlide, d._tempFx = b, d.API.prepareTx(!0, c)
              }
          },
          stop: function () {
              var b = this.opts(),
                c = b.container;
              clearTimeout(b.timeoutId), b.timeoutId = 0, b.API.stopTransition(), b.pauseOnHover && (b.pauseOnHover !== !0 && (c = a(b.pauseOnHover)), c.off("mouseenter mouseleave")), b.API.trigger("cycle-stopped", [b]).log("cycle-stopped")
          },
          reinit: function () {
              var a = this.opts();
              a.API.destroy(), a.container.cycle()
          },
          remove: function (b) {
              for (var c, d, e = this.opts(), f = [], g = 1, h = 0; h < e.slides.length; h++) c = e.slides[h], h == b ? d = c : (f.push(c), a(c).data("cycle.opts").slideNum = g, g++);
              d && (e.slides = a(f), e.slideCount--, a(d).remove(), b == e.currSlide ? e.API.advanceSlide(1) : b < e.currSlide ? e.currSlide-- : e.currSlide++, e.API.trigger("cycle-slide-removed", [e, b, d]).log("cycle-slide-removed"), e.API.updateView())
          }
      }), a(document).on("click.cycle", "[data-cycle-cmd]", function (b) {
          b.preventDefault();
          var c = a(this),
            d = c.data("cycle-cmd"),
            e = c.data("cycle-context") || ".cycle-slideshow";
          a(e).cycle(d, c.data("cycle-arg"))
      })
  }(jQuery),
  function (a) {
      "use strict";

      function b(b, c) {
          var d;
          return b._hashFence ? void (b._hashFence = !1) : (d = window.location.hash.substring(1), void b.slides.each(function (e) {
              if (a(this).data("cycle-hash") == d) {
                  if (c === !0) b.startingSlide = e;
                  else {
                      var f = b.currSlide < e;
                      b.nextSlide = e, b.API.prepareTx(!0, f)
                  }
                  return !1
              }
          }))
      }
      a(document).on("cycle-pre-initialize", function (c, d) {
          b(d, !0), d._onHashChange = function () {
              b(d, !1)
          }, a(window).on("hashchange", d._onHashChange)
      }), a(document).on("cycle-update-view", function (a, b, c) {
          c.hash && "#" + c.hash != window.location.hash && (b._hashFence = !0, window.location.hash = c.hash)
      }), a(document).on("cycle-destroyed", function (b, c) {
          c._onHashChange && a(window).off("hashchange", c._onHashChange)
      })
  }(jQuery),
  function (a) {
      "use strict";
      a.extend(a.fn.cycle.defaults, {
          loader: !1
      }), a(document).on("cycle-bootstrap", function (b, c) {
          function d(b, d) {
              function f(b) {
                  var f;
                  "wait" == c.loader ? (h.push(b), 0 === j && (h.sort(g), e.apply(c.API, [h, d]), c.container.removeClass("cycle-loading"))) : (f = a(c.slides[c.currSlide]), e.apply(c.API, [b, d]), f.show(), c.container.removeClass("cycle-loading"))
              }

              function g(a, b) {
                  return a.data("index") - b.data("index")
              }
              var h = [];
              if ("string" == a.type(b)) b = a.trim(b);
              else if ("array" === a.type(b))
                  for (var i = 0; i < b.length; i++) b[i] = a(b[i])[0];
              b = a(b);
              var j = b.length;
              j && (b.css("visibility", "hidden").appendTo("body").each(function (b) {
                  function g() {
                      0 === --i && (--j, f(k))
                  }
                  var i = 0,
                    k = a(this),
                    l = k.is("img") ? k : k.find("img");
                  return k.data("index", b), l = l.filter(":not(.cycle-loader-ignore)").filter(':not([src=""])'), l.length ? (i = l.length, void l.each(function () {
                      this.complete ? g() : a(this).load(function () {
                          g()
                      }).on("error", function () {
                          0 === --i && (c.API.log("slide skipped; img not loaded:", this.src), 0 === --j && "wait" == c.loader && e.apply(c.API, [h, d]))
                      })
                  })) : (--j, void h.push(k))
              }), j && c.container.addClass("cycle-loading"))
          }
          var e;
          c.loader && (e = c.API.add, c.API.add = d)
      })
  }(jQuery),
  function (a) {
      "use strict";

      function b(b, c, d) {
          var e, f = b.API.getComponent("pager");
          f.each(function () {
              var f = a(this);
              if (c.pagerTemplate) {
                  var g = b.API.tmpl(c.pagerTemplate, c, b, d[0]);
                  e = a(g).appendTo(f)
              } else e = f.children().eq(b.slideCount - 1);
              e.on(b.pagerEvent, function (a) {
                  b.pagerEventBubble || a.preventDefault(), b.API.page(f, a.currentTarget)
              })
          })
      }

      function c(a, b) {
          var c = this.opts();
          if (!c.busy || c.manualTrump) {
              var d = a.children().index(b),
                e = d,
                f = c.currSlide < e;
              c.currSlide != e && (c.nextSlide = e, c._tempFx = c.pagerFx, c.API.prepareTx(!0, f), c.API.trigger("cycle-pager-activated", [c, a, b]))
          }
      }
      a.extend(a.fn.cycle.defaults, {
          pager: "> .cycle-pager",
          pagerActiveClass: "cycle-pager-active",
          pagerEvent: "click.cycle",
          pagerEventBubble: void 0,
          pagerTemplate: "<span>&bull;</span>"
      }), a(document).on("cycle-bootstrap", function (a, c, d) {
          d.buildPagerLink = b
      }), a(document).on("cycle-slide-added", function (a, b, d, e) {
          b.pager && (b.API.buildPagerLink(b, d, e), b.API.page = c)
      }), a(document).on("cycle-slide-removed", function (b, c, d) {
          if (c.pager) {
              var e = c.API.getComponent("pager");
              e.each(function () {
                  var b = a(this);
                  a(b.children()[d]).remove()
              })
          }
      }), a(document).on("cycle-update-view", function (b, c) {
          var d;
          c.pager && (d = c.API.getComponent("pager"), d.each(function () {
              a(this).children().removeClass(c.pagerActiveClass).eq(c.currSlide).addClass(c.pagerActiveClass)
          }))
      }), a(document).on("cycle-destroyed", function (a, b) {
          var c = b.API.getComponent("pager");
          c && (c.children().off(b.pagerEvent), b.pagerTemplate && c.empty())
      })
  }(jQuery),
  function (a) {
      "use strict";
      a.extend(a.fn.cycle.defaults, {
          next: "> .cycle-next",
          nextEvent: "click.cycle",
          disabledClass: "disabled",
          prev: "> .cycle-prev",
          prevEvent: "click.cycle",
          swipe: !1
      }), a(document).on("cycle-initialized", function (a, b) {
          if (b.API.getComponent("next").on(b.nextEvent, function (a) {
              a.preventDefault(), b.API.next()
          }), b.API.getComponent("prev").on(b.prevEvent, function (a) {
              a.preventDefault(), b.API.prev()
          }), b.swipe) {
              var c = b.swipeVert ? "swipeUp.cycle" : "swipeLeft.cycle swipeleft.cycle",
                d = b.swipeVert ? "swipeDown.cycle" : "swipeRight.cycle swiperight.cycle";
              b.container.on(c, function () {
                  b._tempFx = b.swipeFx, b.API.next()
              }), b.container.on(d, function () {
                  b._tempFx = b.swipeFx, b.API.prev()
              })
          }
      }), a(document).on("cycle-update-view", function (a, b) {
          if (!b.allowWrap) {
              var c = b.disabledClass,
                d = b.API.getComponent("next"),
                e = b.API.getComponent("prev"),
                f = b._prevBoundry || 0,
                g = void 0 !== b._nextBoundry ? b._nextBoundry : b.slideCount - 1;
              b.currSlide == g ? d.addClass(c).prop("disabled", !0) : d.removeClass(c).prop("disabled", !1), b.currSlide === f ? e.addClass(c).prop("disabled", !0) : e.removeClass(c).prop("disabled", !1)
          }
      }), a(document).on("cycle-destroyed", function (a, b) {
          b.API.getComponent("prev").off(b.nextEvent), b.API.getComponent("next").off(b.prevEvent), b.container.off("swipeleft.cycle swiperight.cycle swipeLeft.cycle swipeRight.cycle swipeUp.cycle swipeDown.cycle")
      })
  }(jQuery),
  function (a) {
      "use strict";
      a.extend(a.fn.cycle.defaults, {
          progressive: !1
      }),
        a(document).on("cycle-pre-initialize", function (b, c) {
            if (c.progressive) {
                var d, e, f = c.API,
                  g = f.next,
                  h = f.prev,
                  i = f.prepareTx,
                  j = a.type(c.progressive);
                if ("array" == j) d = c.progressive;
                else if (a.isFunction(c.progressive)) d = c.progressive(c);
                else if ("string" == j) {
                    if (e = a(c.progressive), d = a.trim(e.html()), !d) return;
                    if (/^(\[)/.test(d)) try {
                        d = a.parseJSON(d)
                    } catch (k) {
                        return void f.log("error parsing progressive slides", k)
                    } else d = d.split(new RegExp(e.data("cycle-split") || "\n")), d[d.length - 1] || d.pop()
                }
                i && (f.prepareTx = function (a, b) {
                    var e, f;
                    return a || 0 === d.length ? void i.apply(c.API, [a, b]) : void (b && c.currSlide == c.slideCount - 1 ? (f = d[0], d = d.slice(1), c.container.one("cycle-slide-added", function (a, b) {
                        setTimeout(function () {
                            b.API.advanceSlide(1)
                        }, 50)
                    }), c.API.add(f)) : b || 0 !== c.currSlide ? i.apply(c.API, [a, b]) : (e = d.length - 1, f = d[e], d = d.slice(0, e), c.container.one("cycle-slide-added", function (a, b) {
                        setTimeout(function () {
                            b.currSlide = 1, b.API.advanceSlide(-1)
                        }, 50)
                    }), c.API.add(f, !0)))
                }), g && (f.next = function () {
                    var a = this.opts();
                    if (d.length && a.currSlide == a.slideCount - 1) {
                        var b = d[0];
                        d = d.slice(1), a.container.one("cycle-slide-added", function (a, b) {
                            g.apply(b.API), b.container.removeClass("cycle-loading")
                        }), a.container.addClass("cycle-loading"), a.API.add(b)
                    } else g.apply(a.API)
                }), h && (f.prev = function () {
                    var a = this.opts();
                    if (d.length && 0 === a.currSlide) {
                        var b = d.length - 1,
                          c = d[b];
                        d = d.slice(0, b), a.container.one("cycle-slide-added", function (a, b) {
                            b.currSlide = 1, b.API.advanceSlide(-1), b.container.removeClass("cycle-loading")
                        }), a.container.addClass("cycle-loading"), a.API.add(c, !0)
                    } else h.apply(a.API)
                })
            }
        })
  }(jQuery),
  function (a) {
      "use strict";
      a.extend(a.fn.cycle.defaults, {
          tmplRegex: "{{((.)?.*?)}}"
      }), a.extend(a.fn.cycle.API, {
          tmpl: function (b, c) {
              var d = new RegExp(c.tmplRegex || a.fn.cycle.defaults.tmplRegex, "g"),
                e = a.makeArray(arguments);
              return e.shift(), b.replace(d, function (b, c) {
                  var d, f, g, h, i = c.split(".");
                  for (d = 0; d < e.length; d++)
                      if (g = e[d]) {
                          if (i.length > 1)
                              for (h = g, f = 0; f < i.length; f++) g = h, h = h[i[f]] || c;
                          else h = g[c];
                          if (a.isFunction(h)) return h.apply(g, e);
                          if (void 0 !== h && null !== h && h != c) return h
                      }
                  return c
              })
          }
      })
  }(jQuery), ! function (a) {
      "use strict";
      a.fn.cycle.transitions.scrollVert = {
          before: function (a, b, c, d) {
              a.API.stackSlides(a, b, c, d);
              var e = a.container.css("overflow", "hidden").height();
              a.cssBefore = {
                  top: d ? -e : e,
                  left: 0,
                  opacity: 1,
                  display: "block",
                  visibility: "visible"
              }, a.animIn = {
                  top: 0
              }, a.animOut = {
                  top: d ? e : -e
              }
          }
      }
  }(jQuery), ! function (a) {
      "use strict";
      a.extend(a.fn.cycle.defaults, {
          centerHorz: !1,
          centerVert: !1
      }), a(document).on("cycle-pre-initialize", function (b, c) {
          function d() {
              clearTimeout(i), i = setTimeout(g, 50)
          }

          function e() {
              clearTimeout(i), clearTimeout(j), a(window).off("resize orientationchange", d)
          }

          function f() {
              c.slides.each(h)
          }

          function g() {
              h.apply(c.container.find("." + c.slideActiveClass)), clearTimeout(j), j = setTimeout(f, 50)
          }

          function h() {
              var b = a(this),
                d = c.container.width(),
                e = c.container.height(),
                f = b.outerWidth(),
                g = b.outerHeight();
              f && (c.centerHorz && d >= f && b.css("marginLeft", (d - f) / 2), c.centerVert && e >= g && b.css("marginTop", (e - g) / 2))
          }
          if (c.centerHorz || c.centerVert) {
              var i, j;
              a(window).on("resize orientationchange load", d), c.container.on("cycle-destroyed", e), c.container.on("cycle-initialized cycle-slide-added cycle-slide-removed", function () {
                  d()
              }), g()
          }
      })
  }(jQuery),
  function (a) {
      a.easing.jswing = a.easing.swing, a.extend(a.easing, {
          def: "easeOutQuad",
          swing: function (b, c, d, e, f) {
              return a.easing[a.easing.def](b, c, d, e, f)
          },
          easeInQuad: function (a, b, c, d, e) {
              return d * (b /= e) * b + c
          },
          easeOutQuad: function (a, b, c, d, e) {
              return -d * (b /= e) * (b - 2) + c
          },
          easeInOutQuad: function (a, b, c, d, e) {
              return 1 > (b /= e / 2) ? d / 2 * b * b + c : -d / 2 * (--b * (b - 2) - 1) + c
          },
          easeInCubic: function (a, b, c, d, e) {
              return d * (b /= e) * b * b + c
          },
          easeOutCubic: function (a, b, c, d, e) {
              return d * ((b = b / e - 1) * b * b + 1) + c
          },
          easeInOutCubic: function (a, b, c, d, e) {
              return 1 > (b /= e / 2) ? d / 2 * b * b * b + c : d / 2 * ((b -= 2) * b * b + 2) + c
          },
          easeInQuart: function (a, b, c, d, e) {
              return d * (b /= e) * b * b * b + c
          },
          easeOutQuart: function (a, b, c, d, e) {
              return -d * ((b = b / e - 1) * b * b * b - 1) + c
          },
          easeInOutQuart: function (a, b, c, d, e) {
              return 1 > (b /= e / 2) ? d / 2 * b * b * b * b + c : -d / 2 * ((b -= 2) * b * b * b - 2) + c
          },
          easeInQuint: function (a, b, c, d, e) {
              return d * (b /= e) * b * b * b * b + c
          },
          easeOutQuint: function (a, b, c, d, e) {
              return d * ((b = b / e - 1) * b * b * b * b + 1) + c
          },
          easeInOutQuint: function (a, b, c, d, e) {
              return 1 > (b /= e / 2) ? d / 2 * b * b * b * b * b + c : d / 2 * ((b -= 2) * b * b * b * b + 2) + c
          },
          easeInSine: function (a, b, c, d, e) {
              return -d * Math.cos(b / e * (Math.PI / 2)) + d + c
          },
          easeOutSine: function (a, b, c, d, e) {
              return d * Math.sin(b / e * (Math.PI / 2)) + c
          },
          easeInOutSine: function (a, b, c, d, e) {
              return -d / 2 * (Math.cos(Math.PI * b / e) - 1) + c
          },
          easeInExpo: function (a, b, c, d, e) {
              return 0 == b ? c : d * Math.pow(2, 10 * (b / e - 1)) + c
          },
          easeOutExpo: function (a, b, c, d, e) {
              return b == e ? c + d : d * (-Math.pow(2, -10 * b / e) + 1) + c
          },
          easeInOutExpo: function (a, b, c, d, e) {
              return 0 == b ? c : b == e ? c + d : 1 > (b /= e / 2) ? d / 2 * Math.pow(2, 10 * (b - 1)) + c : d / 2 * (-Math.pow(2, -10 * --b) + 2) + c
          },
          easeInCirc: function (a, b, c, d, e) {
              return -d * (Math.sqrt(1 - (b /= e) * b) - 1) + c
          },
          easeOutCirc: function (a, b, c, d, e) {
              return d * Math.sqrt(1 - (b = b / e - 1) * b) + c
          },
          easeInOutCirc: function (a, b, c, d, e) {
              return 1 > (b /= e / 2) ? -d / 2 * (Math.sqrt(1 - b * b) - 1) + c : d / 2 * (Math.sqrt(1 - (b -= 2) * b) + 1) + c
          },
          easeInElastic: function (a, b, c, d, e) {
              a = 1.70158;
              var f = 0,
                g = d;
              return 0 == b ? c : 1 == (b /= e) ? c + d : (f || (f = .3 * e), g < Math.abs(d) ? (g = d, a = f / 4) : a = f / (2 * Math.PI) * Math.asin(d / g), -(g * Math.pow(2, 10 * --b) * Math.sin(2 * (b * e - a) * Math.PI / f)) + c)
          },
          easeOutElastic: function (a, b, c, d, e) {
              a = 1.70158;
              var f = 0,
                g = d;
              return 0 == b ? c : 1 == (b /= e) ? c + d : (f || (f = .3 * e), g < Math.abs(d) ? (g = d, a = f / 4) : a = f / (2 * Math.PI) * Math.asin(d / g), g * Math.pow(2, -10 * b) * Math.sin(2 * (b * e - a) * Math.PI / f) + d + c)
          },
          easeInOutElastic: function (a, b, c, d, e) {
              a = 1.70158;
              var f = 0,
                g = d;
              return 0 == b ? c : 2 == (b /= e / 2) ? c + d : (f || (f = .3 * e * 1.5), g < Math.abs(d) ? (g = d, a = f / 4) : a = f / (2 * Math.PI) * Math.asin(d / g), 1 > b ? -.5 * g * Math.pow(2, 10 * --b) * Math.sin(2 * (b * e - a) * Math.PI / f) + c : g * Math.pow(2, -10 * --b) * Math.sin(2 * (b * e - a) * Math.PI / f) * .5 + d + c)
          },
          easeInBack: function (a, b, c, d, e, f) {
              return void 0 == f && (f = 1.70158), d * (b /= e) * b * ((f + 1) * b - f) + c
          },
          easeOutBack: function (a, b, c, d, e, f) {
              return void 0 == f && (f = 1.70158), d * ((b = b / e - 1) * b * ((f + 1) * b + f) + 1) + c
          },
          easeInOutBack: function (a, b, c, d, e, f) {
              return void 0 == f && (f = 1.70158), 1 > (b /= e / 2) ? d / 2 * b * b * (((f *= 1.525) + 1) * b - f) + c : d / 2 * ((b -= 2) * b * (((f *= 1.525) + 1) * b + f) + 2) + c
          },
          easeInBounce: function (b, c, d, e, f) {
              return e - a.easing.easeOutBounce(b, f - c, 0, e, f) + d
          },
          easeOutBounce: function (a, b, c, d, e) {
              return (b /= e) < 1 / 2.75 ? 7.5625 * d * b * b + c : 2 / 2.75 > b ? d * (7.5625 * (b -= 1.5 / 2.75) * b + .75) + c : 2.5 / 2.75 > b ? d * (7.5625 * (b -= 2.25 / 2.75) * b + .9375) + c : d * (7.5625 * (b -= 2.625 / 2.75) * b + .984375) + c
          },
          easeInOutBounce: function (b, c, d, e, f) {
              return f / 2 > c ? .5 * a.easing.easeInBounce(b, 2 * c, 0, e, f) + d : .5 * a.easing.easeOutBounce(b, 2 * c - f, 0, e, f) + .5 * e + d
          }
      })
  }(jQuery),




  function (a) {
      if ("object" == typeof exports && "undefined" != typeof module) module.exports = a();
      else if ("function" == typeof define && define.amd) define([], a);
      else {
          var b;
          b = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this, b.ProgressBar = a()
      }
  }(function () {
      var a;
      return function b(a, c, d) {
          function e(g, h) {
              if (!c[g]) {
                  if (!a[g]) {
                      var i = "function" == typeof require && require;
                      if (!h && i) return i(g, !0);
                      if (f) return f(g, !0);
                      var j = new Error("Cannot find module '" + g + "'");
                      throw j.code = "MODULE_NOT_FOUND", j
                  }
                  var k = c[g] = {
                      exports: {}
                  };
                  a[g][0].call(k.exports, function (b) {
                      var c = a[g][1][b];
                      return e(c ? c : b)
                  }, k, k.exports, b, a, c, d)
              }
              return c[g].exports
          }
          for (var f = "function" == typeof require && require, g = 0; g < d.length; g++) e(d[g]);
          return e
      }
        ({
            1: [function (b, c, d) {
                (function () {
                    var b = this,
                      e = function () {
                          "use strict";

                          function e() { }

                          function f(a, b) {
                              var c;
                              for (c in a) Object.hasOwnProperty.call(a, c) && b(c)
                          }

                          function g(a, b) {
                              return f(b, function (c) {
                                  a[c] = b[c]
                              }), a
                          }

                          function h(a, b) {
                              f(b, function (c) {
                                  "undefined" == typeof a[c] && (a[c] = b[c])
                              })
                          }

                          function i(a, b, c, d, e, f, g) {
                              var h, i, k, l = f > a ? 0 : (a - f) / e;
                              for (h in b) b.hasOwnProperty(h) && (i = g[h], k = "function" == typeof i ? i : o[i], b[h] = j(c[h], d[h], k, l));
                              return b
                          }

                          function j(a, b, c, d) {
                              return a + (b - a) * c(d)
                          }

                          function k(a, b) {
                              var c = n.prototype.filter,
                                d = a._filterArgs;
                              f(c, function (e) {
                                  "undefined" != typeof c[e][b] && c[e][b].apply(a, d)
                              })
                          }

                          function l(a, b, c, d, e, f, g, h, j, l, m) {
                              v = b + c + d, w = Math.min(m || u(), v), x = w >= v, y = d - (v - w), a.isPlaying() && !x ? (a._scheduleId = l(a._timeoutHandler, s), k(a, "beforeTween"), b + c > w ? i(1, e, f, g, 1, 1, h) : i(w, e, f, g, d, b + c, h), k(a, "afterTween"), j(e, a._attachment, y)) : a.isPlaying() && x && (j(g, a._attachment, y), a.stop(!0))
                          }

                          function m(a, b) {
                              var c = {},
                                d = typeof b;
                              return "string" === d || "function" === d ? f(a, function (a) {
                                  c[a] = b
                              }) : f(a, function (a) {
                                  c[a] || (c[a] = b[a] || q)
                              }), c
                          }

                          function n(a, b) {
                              this._currentState = a || {}, this._configured = !1, this._scheduleFunction = p, "undefined" != typeof b && this.setConfig(b)
                          }
                          var o, p, q = "linear",
                            r = 500,
                            s = 1e3 / 60,
                            t = Date.now ? Date.now : function () {
                                return +new Date
                            },
                            u = "undefined" != typeof SHIFTY_DEBUG_NOW ? SHIFTY_DEBUG_NOW : t;
                          p = "undefined" != typeof window ? window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || window.mozCancelRequestAnimationFrame && window.mozRequestAnimationFrame || setTimeout : setTimeout;
                          var v, w, x, y;
                          return n.prototype.tween = function (a) {
                              return this._isTweening ? this : (void 0 === a && this._configured || this.setConfig(a), this._timestamp = u(), this._start(this.get(), this._attachment), this.resume())
                          }, n.prototype.setConfig = function (a) {
                              a = a || {}, this._configured = !0, this._attachment = a.attachment, this._pausedAtTime = null, this._scheduleId = null, this._delay = a.delay || 0, this._start = a.start || e, this._step = a.step || e, this._finish = a.finish || e, this._duration = a.duration || r, this._currentState = g({}, a.from) || this.get(), this._originalState = this.get(), this._targetState = g({}, a.to) || this.get();
                              var b = this;
                              this._timeoutHandler = function () {
                                  l(b, b._timestamp, b._delay, b._duration, b._currentState, b._originalState, b._targetState, b._easing, b._step, b._scheduleFunction)
                              };
                              var c = this._currentState,
                                d = this._targetState;
                              return h(d, c), this._easing = m(c, a.easing || q), this._filterArgs = [c, this._originalState, d, this._easing], k(this, "tweenCreated"), this
                          }, n.prototype.get = function () {
                              return g({}, this._currentState)
                          }, n.prototype.set = function (a) {
                              this._currentState = a
                          }, n.prototype.pause = function () {
                              return this._pausedAtTime = u(), this._isPaused = !0, this
                          }, n.prototype.resume = function () {
                              return this._isPaused && (this._timestamp += u() - this._pausedAtTime), this._isPaused = !1, this._isTweening = !0, this._timeoutHandler(), this
                          }, n.prototype.seek = function (a) {
                              a = Math.max(a, 0);
                              var b = u();
                              return this._timestamp + a === 0 ? this : (this._timestamp = b - a, this.isPlaying() || (this._isTweening = !0, this._isPaused = !1, l(this, this._timestamp, this._delay, this._duration, this._currentState, this._originalState, this._targetState, this._easing, this._step, this._scheduleFunction, b), this.pause()), this)
                          }, n.prototype.stop = function (a) {
                              return this._isTweening = !1, this._isPaused = !1, this._timeoutHandler = e, (b.cancelAnimationFrame || b.webkitCancelAnimationFrame || b.oCancelAnimationFrame || b.msCancelAnimationFrame || b.mozCancelRequestAnimationFrame || b.clearTimeout)(this._scheduleId), a && (k(this, "beforeTween"), i(1, this._currentState, this._originalState, this._targetState, 1, 0, this._easing), k(this, "afterTween"), k(this, "afterTweenEnd"), this._finish.call(this, this._currentState, this._attachment)), this
                          }, n.prototype.isPlaying = function () {
                              return this._isTweening && !this._isPaused
                          }, n.prototype.setScheduleFunction = function (a) {
                              this._scheduleFunction = a
                          }, n.prototype.dispose = function () {
                              var a;
                              for (a in this) this.hasOwnProperty(a) && delete this[a]
                          }, n.prototype.filter = {}, n.prototype.formula = {
                              linear: function (a) {
                                  return a
                              }
                          }, o = n.prototype.formula, g(n, {
                              now: u,
                              each: f,
                              tweenProps: i,
                              tweenProp: j,
                              applyFilter: k,
                              shallowCopy: g,
                              defaults: h,
                              composeEasingObject: m
                          }), "function" == typeof SHIFTY_DEBUG_NOW && (b.timeoutHandler = l), "object" == typeof d ? c.exports = n : "function" == typeof a && a.amd ? a(function () {
                              return n
                          }) : "undefined" == typeof b.Tweenable && (b.Tweenable = n), n
                      }();
                    ! function () {
                        e.shallowCopy(e.prototype.formula, {
                            easeInQuad: function (a) {
                                return Math.pow(a, 2)
                            },
                            easeOutQuad: function (a) {
                                return -(Math.pow(a - 1, 2) - 1)
                            },
                            easeInOutQuad: function (a) {
                                return (a /= .5) < 1 ? .5 * Math.pow(a, 2) : -.5 * ((a -= 2) * a - 2)
                            },
                            easeInCubic: function (a) {
                                return Math.pow(a, 3)
                            },
                            easeOutCubic: function (a) {
                                return Math.pow(a - 1, 3) + 1
                            },
                            easeInOutCubic: function (a) {
                                return (a /= .5) < 1 ? .5 * Math.pow(a, 3) : .5 * (Math.pow(a - 2, 3) + 2)
                            },
                            easeInQuart: function (a) {
                                return Math.pow(a, 4)
                            },
                            easeOutQuart: function (a) {
                                return -(Math.pow(a - 1, 4) - 1)
                            },
                            easeInOutQuart: function (a) {
                                return (a /= .5) < 1 ? .5 * Math.pow(a, 4) : -.5 * ((a -= 2) * Math.pow(a, 3) - 2)
                            },
                            easeInQuint: function (a) {
                                return Math.pow(a, 5)
                            },
                            easeOutQuint: function (a) {
                                return Math.pow(a - 1, 5) + 1
                            },
                            easeInOutQuint: function (a) {
                                return (a /= .5) < 1 ? .5 * Math.pow(a, 5) : .5 * (Math.pow(a - 2, 5) + 2)
                            },
                            easeInSine: function (a) {
                                return -Math.cos(a * (Math.PI / 2)) + 1
                            },
                            easeOutSine: function (a) {
                                return Math.sin(a * (Math.PI / 2))
                            },
                            easeInOutSine: function (a) {
                                return -.5 * (Math.cos(Math.PI * a) - 1)
                            },
                            easeInExpo: function (a) {
                                return 0 === a ? 0 : Math.pow(2, 10 * (a - 1))
                            },
                            easeOutExpo: function (a) {
                                return 1 === a ? 1 : -Math.pow(2, -10 * a) + 1
                            },
                            easeInOutExpo: function (a) {
                                return 0 === a ? 0 : 1 === a ? 1 : (a /= .5) < 1 ? .5 * Math.pow(2, 10 * (a - 1)) : .5 * (-Math.pow(2, -10 * --a) + 2)
                            },
                            easeInCirc: function (a) {
                                return -(Math.sqrt(1 - a * a) - 1)
                            },
                            easeOutCirc: function (a) {
                                return Math.sqrt(1 - Math.pow(a - 1, 2))
                            },
                            easeInOutCirc: function (a) {
                                return (a /= .5) < 1 ? -.5 * (Math.sqrt(1 - a * a) - 1) : .5 * (Math.sqrt(1 - (a -= 2) * a) + 1)
                            },
                            easeOutBounce: function (a) {
                                return 1 / 2.75 > a ? 7.5625 * a * a : 2 / 2.75 > a ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : 2.5 / 2.75 > a ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375
                            },
                            easeInBack: function (a) {
                                var b = 1.70158;
                                return a * a * ((b + 1) * a - b)
                            },
                            easeOutBack: function (a) {
                                var b = 1.70158;
                                return (a -= 1) * a * ((b + 1) * a + b) + 1
                            },
                            easeInOutBack: function (a) {
                                var b = 1.70158;
                                return (a /= .5) < 1 ? .5 * a * a * (((b *= 1.525) + 1) * a - b) : .5 * ((a -= 2) * a * (((b *= 1.525) + 1) * a + b) + 2);

                            },
                            elastic: function (a) {
                                return -1 * Math.pow(4, -8 * a) * Math.sin(2 * (6 * a - 1) * Math.PI / 2) + 1
                            },
                            swingFromTo: function (a) {
                                var b = 1.70158;
                                return (a /= .5) < 1 ? .5 * a * a * (((b *= 1.525) + 1) * a - b) : .5 * ((a -= 2) * a * (((b *= 1.525) + 1) * a + b) + 2)
                            },
                            swingFrom: function (a) {
                                var b = 1.70158;
                                return a * a * ((b + 1) * a - b)
                            },
                            swingTo: function (a) {
                                var b = 1.70158;
                                return (a -= 1) * a * ((b + 1) * a + b) + 1
                            },
                            bounce: function (a) {
                                return 1 / 2.75 > a ? 7.5625 * a * a : 2 / 2.75 > a ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : 2.5 / 2.75 > a ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375
                            },
                            bouncePast: function (a) {
                                return 1 / 2.75 > a ? 7.5625 * a * a : 2 / 2.75 > a ? 2 - (7.5625 * (a -= 1.5 / 2.75) * a + .75) : 2.5 / 2.75 > a ? 2 - (7.5625 * (a -= 2.25 / 2.75) * a + .9375) : 2 - (7.5625 * (a -= 2.625 / 2.75) * a + .984375)
                            },
                            easeFromTo: function (a) {
                                return (a /= .5) < 1 ? .5 * Math.pow(a, 4) : -.5 * ((a -= 2) * Math.pow(a, 3) - 2)
                            },
                            easeFrom: function (a) {
                                return Math.pow(a, 4)
                            },
                            easeTo: function (a) {
                                return Math.pow(a, .25)
                            }
                        })
                    }(),
                    function () {
                        function a(a, b, c, d, e, f) {
                            function g(a) {
                                return ((n * a + o) * a + p) * a
                            }

                            function h(a) {
                                return ((q * a + r) * a + s) * a
                            }

                            function i(a) {
                                return (3 * n * a + 2 * o) * a + p
                            }

                            function j(a) {
                                return 1 / (200 * a)
                            }

                            function k(a, b) {
                                return h(m(a, b))
                            }

                            function l(a) {
                                return a >= 0 ? a : 0 - a
                            }

                            function m(a, b) {
                                var c, d, e, f, h, j;
                                for (e = a, j = 0; 8 > j; j++) {
                                    if (f = g(e) - a, l(f) < b) return e;
                                    if (h = i(e), l(h) < 1e-6) break;
                                    e -= f / h
                                }
                                if (c = 0, d = 1, e = a, c > e) return c;
                                if (e > d) return d;
                                for (; d > c;) {
                                    if (f = g(e), l(f - a) < b) return e;
                                    a > f ? c = e : d = e, e = .5 * (d - c) + c
                                }
                                return e
                            }
                            var n = 0,
                              o = 0,
                              p = 0,
                              q = 0,
                              r = 0,
                              s = 0;
                            return p = 3 * b, o = 3 * (d - b) - p, n = 1 - p - o, s = 3 * c, r = 3 * (e - c) - s, q = 1 - s - r, k(a, j(f))
                        }

                        function b(b, c, d, e) {
                            return function (f) {
                                return a(f, b, c, d, e, 1)
                            }
                        }
                        e.setBezierFunction = function (a, c, d, f, g) {
                            var h = b(c, d, f, g);
                            return h.displayName = a, h.x1 = c, h.y1 = d, h.x2 = f, h.y2 = g, e.prototype.formula[a] = h
                        }, e.unsetBezierFunction = function (a) {
                            delete e.prototype.formula[a]
                        }
                    }(),
                    function () {
                        function a(a, b, c, d, f, g) {
                            return e.tweenProps(d, b, a, c, 1, g, f)
                        }
                        var b = new e;
                        b._filterArgs = [], e.interpolate = function (c, d, f, g, h) {
                            var i = e.shallowCopy({}, c),
                              j = h || 0,
                              k = e.composeEasingObject(c, g || "linear");
                            b.set({});
                            var l = b._filterArgs;
                            l.length = 0, l[0] = i, l[1] = c, l[2] = d, l[3] = k, e.applyFilter(b, "tweenCreated"), e.applyFilter(b, "beforeTween");
                            var m = a(c, i, d, f, k, j);
                            return e.applyFilter(b, "afterTween"), m
                        }
                    }(),
                    function (a) {
                        function b(a, b) {
                            var c, d = [],
                              e = a.length;
                            for (c = 0; e > c; c++) d.push("_" + b + "_" + c);
                            return d
                        }

                        function c(a) {
                            var b = a.match(v);
                            return b ? (1 === b.length || a[0].match(u)) && b.unshift("") : b = ["", ""], b.join(A)
                        }

                        function d(b) {
                            a.each(b, function (a) {
                                var c = b[a];
                                "string" == typeof c && c.match(z) && (b[a] = e(c))
                            })
                        }

                        function e(a) {
                            return i(z, a, f)
                        }

                        function f(a) {
                            var b = g(a);
                            return "rgb(" + b[0] + "," + b[1] + "," + b[2] + ")"
                        }

                        function g(a) {
                            return a = a.replace(/#/, ""), 3 === a.length && (a = a.split(""), a = a[0] + a[0] + a[1] + a[1] + a[2] + a[2]), B[0] = h(a.substr(0, 2)), B[1] = h(a.substr(2, 2)), B[2] = h(a.substr(4, 2)), B
                        }

                        function h(a) {
                            return parseInt(a, 16)
                        }

                        function i(a, b, c) {
                            var d = b.match(a),
                              e = b.replace(a, A);
                            if (d)
                                for (var f, g = d.length, h = 0; g > h; h++) f = d.shift(), e = e.replace(A, c(f));
                            return e
                        }

                        function j(a) {
                            return i(x, a, k)
                        }

                        function k(a) {
                            for (var b = a.match(w), c = b.length, d = a.match(y)[0], e = 0; c > e; e++) d += parseInt(b[e], 10) + ",";
                            return d = d.slice(0, -1) + ")"
                        }

                        function l(d) {
                            var e = {};
                            return a.each(d, function (a) {
                                var f = d[a];
                                if ("string" == typeof f) {
                                    var g = r(f);
                                    e[a] = {
                                        formatString: c(f),
                                        chunkNames: b(g, a)
                                    }
                                }
                            }), e
                        }

                        function m(b, c) {
                            a.each(c, function (a) {
                                for (var d = b[a], e = r(d), f = e.length, g = 0; f > g; g++) b[c[a].chunkNames[g]] = +e[g];
                                delete b[a]
                            })
                        }

                        function n(b, c) {
                            a.each(c, function (a) {
                                var d = b[a],
                                  e = o(b, c[a].chunkNames),
                                  f = p(e, c[a].chunkNames);
                                d = q(c[a].formatString, f), b[a] = j(d)
                            })
                        }

                        function o(a, b) {
                            for (var c, d = {}, e = b.length, f = 0; e > f; f++) c = b[f], d[c] = a[c], delete a[c];
                            return d
                        }

                        function p(a, b) {
                            C.length = 0;
                            for (var c = b.length, d = 0; c > d; d++) C.push(a[b[d]]);
                            return C
                        }

                        function q(a, b) {
                            for (var c = a, d = b.length, e = 0; d > e; e++) c = c.replace(A, +b[e].toFixed(4));
                            return c
                        }

                        function r(a) {
                            return a.match(w)
                        }

                        function s(b, c) {
                            a.each(c, function (a) {
                                var d, e = c[a],
                                  f = e.chunkNames,
                                  g = f.length,
                                  h = b[a];
                                if ("string" == typeof h) {
                                    var i = h.split(" "),
                                      j = i[i.length - 1];
                                    for (d = 0; g > d; d++) b[f[d]] = i[d] || j
                                } else
                                    for (d = 0; g > d; d++) b[f[d]] = h;
                                delete b[a]
                            })
                        }

                        function t(b, c) {
                            a.each(c, function (a) {
                                var d = c[a],
                                  e = d.chunkNames,
                                  f = e.length,
                                  g = b[e[0]],
                                  h = typeof g;
                                if ("string" === h) {
                                    for (var i = "", j = 0; f > j; j++) i += " " + b[e[j]], delete b[e[j]];
                                    b[a] = i.substr(1)
                                } else b[a] = g
                            })
                        }
                        var u = /(\d|\-|\.)/,
                          v = /([^\-0-9\.]+)/g,
                          w = /[0-9.\-]+/g,
                          x = new RegExp("rgb\\(" + w.source + /,\s*/.source + w.source + /,\s*/.source + w.source + "\\)", "g"),
                          y = /^.*\(/,
                          z = /#([0-9]|[a-f]){3,6}/gi,
                          A = "VAL",
                          B = [],
                          C = [];
                        a.prototype.filter.token = {
                            tweenCreated: function (a, b, c) {
                                d(a), d(b), d(c), this._tokenData = l(a)
                            },
                            beforeTween: function (a, b, c, d) {
                                s(d, this._tokenData), m(a, this._tokenData), m(b, this._tokenData), m(c, this._tokenData)
                            },
                            afterTween: function (a, b, c, d) {
                                n(a, this._tokenData), n(b, this._tokenData), n(c, this._tokenData), t(d, this._tokenData)
                            }
                        }
                    }(e)
                }).call(null)
            }, {}],
            2: [function (a, b) {
                var c = a("./shape"),
                  d = a("./utils"),
                  e = function () {
                      this._pathTemplate = "M 50,50 m 0,-{radius} a {radius},{radius} 0 1 1 0,{2radius} a {radius},{radius} 0 1 1 0,-{2radius}", c.apply(this, arguments)
                  };
                e.prototype = new c, e.prototype.constructor = e, e.prototype._pathString = function (a) {
                    var b = a.strokeWidth;
                    a.trailWidth && a.trailWidth > a.strokeWidth && (b = a.trailWidth);
                    var c = 50 - b / 2;
                    return d.render(this._pathTemplate, {
                        radius: c,
                        "2radius": 2 * c
                    })
                }, e.prototype._trailString = function (a) {
                    return this._pathString(a)
                }, b.exports = e
            }, {
                "./shape": 7,
                "./utils": 8
            }],
            3: [function (a, b) {
                var c = a("./shape"),
                  d = a("./utils"),
                  e = function () {
                      this._pathTemplate = "M 0,{center} L 100,{center}", c.apply(this, arguments)
                  };
                e.prototype = new c, e.prototype.constructor = e, e.prototype._initializeSvg = function (a, b) {
                    a.setAttribute("viewBox", "0 0 100 " + b.strokeWidth), a.setAttribute("preserveAspectRatio", "none")
                }, e.prototype._pathString = function (a) {
                    return d.render(this._pathTemplate, {
                        center: a.strokeWidth / 2
                    })
                }, e.prototype._trailString = function (a) {
                    return this._pathString(a)
                }, b.exports = e
            }, {
                "./shape": 7,
                "./utils": 8
            }],
            4: [function (a, b) {
                b.exports = {
                    Line: a("./line"),
                    Circle: a("./circle"),
                    SemiCircle: a("./semicircle"),
                    Path: a("./path"),
                    Shape: a("./shape"),
                    utils: a("./utils")
                }
            }, {
                "./circle": 2,
                "./line": 3,
                "./path": 5,
                "./semicircle": 6,
                "./shape": 7,
                "./utils": 8
            }],
            5: [function (a, b) {
                var c = a("shifty"),
                  d = a("./utils"),
                  e = {
                      easeIn: "easeInCubic",
                      easeOut: "easeOutCubic",
                      easeInOut: "easeInOutCubic"
                  },
                  f = function (a, b) {
                      b = d.extend({
                          duration: 800,
                          easing: "linear",
                          from: {},
                          to: {},
                          step: function () { }
                      }, b);
                      var c;
                      c = d.isString(a) ? document.querySelector(a) : a, this.path = c, this._opts = b, this._tweenable = null;
                      var e = this.path.getTotalLength();
                      this.path.style.strokeDasharray = e + " " + e, this.set(0)
                  };
                f.prototype.value = function () {
                    var a = -(this._getComputedDashOffset()),
                      b = this.path.getTotalLength(),
                      c = 1 - a / b;
                    return parseFloat(c.toFixed(6), 10)
                }, f.prototype.set = function (a) {
                    this.stop(), this.path.style.strokeDashoffset = this._progressToOffset(a);
                    var b = this._opts.step;
                    if (d.isFunction(b)) {
                        var c = this._easing(this._opts.easing),
                          e = this._calculateTo(a, c),
                          f = this._opts.shape || this;
                        b(e, f, this._opts.attachment)
                    }
                }, f.prototype.stop = function () {
                    this._stopTween(), this.path.style.strokeDashoffset = this._getComputedDashOffset()
                }, f.prototype.animate = function (a, b, e) {
                    b = b || {}, d.isFunction(b) && (e = b, b = {});
                    var f = d.extend({}, b),
                      g = d.extend({}, this._opts);
                    b = d.extend(g, b);
                    var h = this._easing(b.easing),
                      i = this._resolveFromAndTo(a, h, f);
                    this.stop(), this.path.getBoundingClientRect();
                    var j = this._getComputedDashOffset(),
                      k = this._progressToOffset(a),
                      l = this;
                    this._tweenable = new c, this._tweenable.tween({
                        from: d.extend({
                            offset: j
                        }, i.from),
                        to: d.extend({
                            offset: k
                        }, i.to),
                        duration: b.duration,
                        easing: h,
                        step: function (a) {
                            l.path.style.strokeDashoffset = a.offset;
                            var c = b.shape || l;
                            b.step(a, c, b.attachment)
                        },
                        finish: function () {
                            d.isFunction(e) && e()
                        }
                    })
                }, f.prototype._getComputedDashOffset = function () {
                    var a = window.getComputedStyle(this.path, null);
                    return parseFloat(a.getPropertyValue("stroke-dashoffset"), -10)
                    //return parseFloat(-600, 10)
                }, f.prototype._progressToOffset = function (a) {
                    var b = this.path.getTotalLength();
                    return b - a * b
                }, f.prototype._resolveFromAndTo = function (a, b, c) {
                    return c.from && c.to ? {
                        from: c.from,
                        to: c.to
                    } : {
                        from: this._calculateFrom(b),
                        to: this._calculateTo(a, b)
                    }
                }, f.prototype._calculateFrom = function (a) {
                    return c.interpolate(this._opts.from, this._opts.to, this.value(), a)
                }, f.prototype._calculateTo = function (a, b) {
                    return c.interpolate(this._opts.from, this._opts.to, a, b)
                }, f.prototype._stopTween = function () {
                    null !== this._tweenable && (this._tweenable.stop(), this._tweenable.dispose(), this._tweenable = null)
                }, f.prototype._easing = function (a) {
                    return e.hasOwnProperty(a) ? e[a] : a
                }, b.exports = f
            }, {
                "./utils": 8,
                shifty: 1
            }],
            6: [function (a, b) {
                var c = a("./shape"),
                  d = a("./circle"),
                  e = a("./utils"),
                  f = function () {
                      this._pathTemplate = "M 50,50 m -{radius},0 a {radius},{radius} 0 1 1 {2radius},0", c.apply(this, arguments)
                  };
                f.prototype = new c, f.prototype.constructor = f, f.prototype._initializeSvg = function (a) {
                    a.setAttribute("viewBox", "0 0 100 50")
                }, f.prototype._initializeTextElement = function (a, b, c) {
                    a.text.style && (c.style.top = "auto", c.style.bottom = "0", a.text.alignToBottom ? e.setStyle(c, "transform", "translate(-50%, 0)") : e.setStyle(c, "transform", "translate(-50%, 50%)"))
                }, f.prototype._pathString = d.prototype._pathString, f.prototype._trailString = d.prototype._trailString, b.exports = f
            }, {
                "./circle": 2,
                "./shape": 7,
                "./utils": 8
            }],
            7: [function (a, b) {
                var c = a("./path"),
                  d = a("./utils"),
                  e = "Object is destroyed",
                  f = function g(a, b) {
                      if (!(this instanceof g)) throw new Error("Constructor was called without new keyword");
                      if (0 !== arguments.length) {
                          this._opts = d.extend({
                              color: "#555",
                              strokeWidth: 1,
                              trailColor: null,
                              trailWidth: null,
                              fill: null,
                              text: {
                                  style: {
                                      color: null,
                                      position: "absolute",
                                      left: "50%",
                                      top: "50%",
                                      padding: 0,
                                      margin: 0,
                                      transform: {
                                          prefix: !0,
                                          value: "translate(-50%, -50%)"
                                      }
                                  },
                                  alignToBottom: !0,
                                  value: "",
                                  className: "progressbar-text"
                              },
                              svgStyle: {
                                  display: "block",
                                  width: "100%"
                              }
                          }, b, !0);
                          var e, f = this._createSvgView(this._opts);
                          if (e = d.isString(a) ? document.querySelector(a) : a, !e) throw new Error("Container does not exist: " + a);
                          this._container = e, this._container.appendChild(f.svg), this._opts.svgStyle && d.setStyles(f.svg, this._opts.svgStyle), this.text = null, this._opts.text.value && (this.text = this._createTextElement(this._opts, this._container), this._container.appendChild(this.text)), this.svg = f.svg, this.path = f.path, this.trail = f.trail;
                          var h = d.extend({
                              attachment: void 0,
                              shape: this
                          }, this._opts);
                          this._progressPath = new c(f.path, h)
                      }
                  };
                f.prototype.animate = function (a, b, c) {
                    if (null === this._progressPath) throw new Error(e);
                    this._progressPath.animate(a, b, c)
                }, f.prototype.stop = function () {
                    if (null === this._progressPath) throw new Error(e);
                    void 0 !== this._progressPath && this._progressPath.stop()
                }, f.prototype.destroy = function () {
                    if (null === this._progressPath) throw new Error(e);
                    this.stop(), this.svg.parentNode.removeChild(this.svg), this.svg = null, this.path = null, this.trail = null, this._progressPath = null, null !== this.text && (this.text.parentNode.removeChild(this.text), this.text = null)
                }, f.prototype.set = function (a) {
                    if (null === this._progressPath) throw new Error(e);
                    this._progressPath.set(a)
                }, f.prototype.value = function () {
                    if (null === this._progressPath) throw new Error(e);
                    return void 0 === this._progressPath ? 0 : this._progressPath.value()
                }, f.prototype.setText = function (a) {
                    if (null === this._progressPath) throw new Error(e);
                    null === this.text && (this.text = this._createTextElement(this._opts, this._container), this._container.appendChild(this.text)), this.text.removeChild(this.text.firstChild), this.text.appendChild(document.createTextNode(a))
                }, f.prototype._createSvgView = function (a) {
                    var b = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                    this._initializeSvg(b, a);
                    var c = null;
                    (a.trailColor || a.trailWidth) && (c = this._createTrail(a), b.appendChild(c));
                    var d = this._createPath(a);
                    return b.appendChild(d), {
                        svg: b,
                        path: d,
                        trail: c
                    }
                }, f.prototype._initializeSvg = function (a) {
                    a.setAttribute("viewBox", "0 0 100 100")
                }, f.prototype._createPath = function (a) {
                    var b = this._pathString(a);
                    return this._createPathElement(b, a)
                }, f.prototype._createTrail = function (a) {
                    var b = this._trailString(a),
                      c = d.extend({}, a);
                    return c.trailColor || (c.trailColor = "#eee"), c.trailWidth || (c.trailWidth = c.strokeWidth), c.color = c.trailColor, c.strokeWidth = c.trailWidth, c.fill = null, this._createPathElement(b, c)
                }, f.prototype._createPathElement = function (a, b) {
                    var c = document.createElementNS("http://www.w3.org/2000/svg", "path");
                    return c.setAttribute("d", a), c.setAttribute("stroke", b.color), c.setAttribute("stroke-width", b.strokeWidth), b.fill ? c.setAttribute("fill", b.fill) : c.setAttribute("fill-opacity", "0"), c
                }, f.prototype._createTextElement = function (a, b) {
                    var c = document.createElement("p");
                    c.appendChild(document.createTextNode(a.text.value));
                    var e = a.text.style;
                    return e && (b.style.position = "relative", d.setStyles(c, e), e.color || (c.style.color = a.color)), c.className = a.text.className, this._initializeTextElement(a, b, c), c
                }, f.prototype._initializeTextElement = function () { }, f.prototype._pathString = function () {
                    throw new Error("Override this function for each progress bar")
                }, f.prototype._trailString = function () {
                    throw new Error("Override this function for each progress bar")
                }, b.exports = f
            }, {
                "./path": 5,
                "./utils": 8
            }],
            8: [function (a, b) {
                function c(a, b, d) {
                    a = a || {}, b = b || {}, d = d || !1;
                    for (var e in b)
                        if (b.hasOwnProperty(e)) {
                            var f = a[e],
                              g = b[e];
                            a[e] = d && k(f) && k(g) ? c(f, g, d) : g
                        }
                    return a
                }

                function d(a, b) {
                    var c = a;
                    for (var d in b)
                        if (b.hasOwnProperty(d)) {
                            var e = b[d],
                              f = "\\{" + d + "\\}",
                              g = new RegExp(f, "g");
                            c = c.replace(g, e)
                        }
                    return c
                }

                function e(a, b, c) {
                    for (var d = 0; d < m.length; ++d) {
                        var e = m[d];
                        a.style[e + g(b)] = c
                    }
                    a.style[b] = c
                }

                function f(a, b) {
                    l(b, function (b, c) {
                        null !== b && void 0 !== b && (k(b) && b.prefix === !0 ? e(a, c, b.value) : a.style[c] = b)
                    })
                }

                function g(a) {
                    return a.charAt(0).toUpperCase() + a.slice(1)
                }

                function h(a) {
                    return "string" == typeof a || a instanceof String
                }

                function i(a) {
                    return "function" == typeof a
                }

                function j(a) {
                    return "[object Array]" === Object.prototype.toString.call(a)
                }

                function k(a) {
                    if (j(a)) return !1;
                    var b = typeof a;
                    return "object" === b && !!a
                }

                function l(a, b) {
                    for (var c in a)
                        if (a.hasOwnProperty(c)) {
                            var d = a[c];
                            b(d, c)
                        }
                }
                var m = "Webkit Moz O ms".split(" ");
                b.exports = {
                    extend: c,
                    render: d,
                    setStyle: e,
                    setStyles: f,
                    capitalize: g,
                    isString: h,
                    isFunction: i,
                    isObject: k,
                    forEachObject: l
                }
            }, {}]
        }, {}, [4])(4)
  }),
  function (a) {
      "use strict";
      var b = -1,
        c = {
            onVisible: function (a) {
                var b = c.isSupported();
                if (!b || !c.hidden()) return a(), b;
                var d = c.change(function () {
                    c.hidden() || (c.unbind(d), a())
                });
                return d
            },
            change: function (a) {
                if (!c.isSupported()) return !1;
                b += 1;
                var d = b;
                return c._callbacks[d] = a, c._listen(), d
            },
            unbind: function (a) {
                delete c._callbacks[a]
            },
            afterPrerendering: function (a) {
                var b = c.isSupported(),
                  d = "prerender";
                if (!b || d != c.state()) return a(), b;
                var e = c.change(function (b, f) {
                    d != f && (c.unbind(e), a())
                });
                return e
            },
            hidden: function () {
                return !(!c._doc.hidden && !c._doc.webkitHidden)
            },
            state: function () {
                return c._doc.visibilityState || c._doc.webkitVisibilityState || "visible"
            },
            isSupported: function () {
                return !(!c._doc.visibilityState && !c._doc.webkitVisibilityState)
            },
            _doc: document || {},
            _callbacks: {},
            _change: function (a) {
                var b = c.state();
                for (var d in c._callbacks) c._callbacks[d].call(c._doc, a, b)
            },
            _listen: function () {
                if (!c._init) {
                    var a = "visibilitychange";
                    c._doc.webkitVisibilityState && (a = "webkit" + a);
                    var b = function () {
                        c._change.apply(c, arguments)
                    };
                    c._doc.addEventListener ? c._doc.addEventListener(a, b) : c._doc.attachEvent(a, b), c._init = !0
                }
            }
        };
      "undefined" != typeof module && module.exports ? module.exports = c : a.Visibility = c
  }(this),
  function (a) {
      "use strict";
      var b = -1,
        c = function (c) {
            return c.every = function (a, d, e) {
                c._time(), e || (e = d, d = null), b += 1;
                var f = b;
                return c._timers[f] = {
                    visible: a,
                    hidden: d,
                    callback: e
                }, c._run(f, !1), c.isSupported() && c._listen(), f
            }, c.stop = function (a) {
                return c._timers[a] ? (c._stop(a), delete c._timers[a], !0) : !1
            }, c._timers = {}, c._time = function () {
                c._timed || (c._timed = !0, c._wasHidden = c.hidden(), c.change(function () {
                    c._stopRun(), c._wasHidden = c.hidden()
                }))
            }, c._run = function (b, d) {
                var e, f = c._timers[b];
                if (c.hidden()) {
                    if (null === f.hidden) return;
                    e = f.hidden
                } else e = f.visible;
                var g = function () {
                    f.last = new Date, f.callback.call(a)
                };
                if (d) {
                    var h = new Date,
                      i = h - f.last;
                    e > i ? f.delay = setTimeout(function () {
                        f.id = setInterval(g, e), g()
                    }, e - i) : (f.id = setInterval(g, e), g())
                } else f.id = setInterval(g, e)
            }, c._stop = function (a) {
                var b = c._timers[a];
                clearInterval(b.id), clearTimeout(b.delay), delete b.id, delete b.delay
            }, c._stopRun = function () {
                var a = c.hidden(),
                  b = c._wasHidden;
                if (a && !b || !a && b)
                    for (var d in c._timers) c._stop(d), c._run(d, !a)
            }, c
        };
      "undefined" != typeof module && module.exports ? module.exports = c(require("./visibility.core")) : c(a.Visibility)
  }(window),
  function () {
      "use strict";

      function a(d) {
          if (!d) throw new Error("No options passed to Waypoint constructor");
          if (!d.element) throw new Error("No element option passed to Waypoint constructor");
          if (!d.handler) throw new Error("No handler option passed to Waypoint constructor");
          this.key = "waypoint-" + b, this.options = a.Adapter.extend({}, a.defaults, d), this.element = this.options.element, this.adapter = new a.Adapter(this.element), this.callback = d.handler, this.axis = this.options.horizontal ? "horizontal" : "vertical", this.enabled = this.options.enabled, this.triggerPoint = null, this.group = a.Group.findOrCreate({
              name: this.options.group,
              axis: this.axis
          }), this.context = a.Context.findOrCreateByElement(this.options.context), a.offsetAliases[this.options.offset] && (this.options.offset = a.offsetAliases[this.options.offset]), this.group.add(this), this.context.add(this), c[this.key] = this, b += 1
      }
      var b = 0,
        c = {};
      a.prototype.queueTrigger = function (a) {
          this.group.queueTrigger(this, a)
      }, a.prototype.trigger = function (a) {
          this.enabled && this.callback && this.callback.apply(this, a)
      }, a.prototype.destroy = function () {
          this.context.remove(this), this.group.remove(this), delete c[this.key]
      }, a.prototype.disable = function () {
          return this.enabled = !1, this
      }, a.prototype.enable = function () {
          return this.context.refresh(), this.enabled = !0, this
      }, a.prototype.next = function () {
          return this.group.next(this)
      }, a.prototype.previous = function () {
          return this.group.previous(this)
      }, a.invokeAll = function (a) {
          var b = [];
          for (var d in c) b.push(c[d]);
          for (var e = 0, f = b.length; f > e; e++) b[e][a]()
      }, a.destroyAll = function () {
          a.invokeAll("destroy")
      }, a.disableAll = function () {
          a.invokeAll("disable")
      }, a.enableAll = function () {
          a.invokeAll("enable")
      }, a.refreshAll = function () {
          a.Context.refreshAll()
      }, a.viewportHeight = function () {
          return window.innerHeight || document.documentElement.clientHeight
      }, a.viewportWidth = function () {
          return document.documentElement.clientWidth
      }, a.adapters = [], a.defaults = {
          context: window,
          continuous: !0,
          enabled: !0,
          group: "default",
          horizontal: !1,
          offset: 0
      }, a.offsetAliases = {
          "bottom-in-view": function () {
              return this.context.innerHeight() - this.adapter.outerHeight()
          },
          "right-in-view": function () {
              return this.context.innerWidth() - this.adapter.outerWidth()
          }
      }, window.Waypoint = a
  }(),
  function () {
      "use strict";

      function a(a) {
          window.setTimeout(a, 1e3 / 60)
      }

      function b(a) {
          this.element = a, this.Adapter = e.Adapter, this.adapter = new this.Adapter(a), this.key = "waypoint-context-" + c, this.didScroll = !1, this.didResize = !1, this.oldScroll = {
              x: this.adapter.scrollLeft(),
              y: this.adapter.scrollTop()
          }, this.waypoints = {
              vertical: {},
              horizontal: {}
          }, a.waypointContextKey = this.key, d[a.waypointContextKey] = this, c += 1, this.createThrottledScrollHandler(), this.createThrottledResizeHandler()
      }
      var c = 0,
        d = {},
        e = window.Waypoint,
        f = window.onload;
      b.prototype.add = function (a) {
          var b = a.options.horizontal ? "horizontal" : "vertical";
          this.waypoints[b][a.key] = a, this.refresh()
      }, b.prototype.checkEmpty = function () {
          var a = this.Adapter.isEmptyObject(this.waypoints.horizontal),
            b = this.Adapter.isEmptyObject(this.waypoints.vertical);
          a && b && (this.adapter.off(".waypoints"), delete d[this.key])
      }, b.prototype.createThrottledResizeHandler = function () {
          function a() {
              b.handleResize(), b.didResize = !1
          }
          var b = this;
          this.adapter.on("resize.waypoints", function () {
              b.didResize || (b.didResize = !0, e.requestAnimationFrame(a))
          })
      }, b.prototype.createThrottledScrollHandler = function () {
          function a() {
              b.handleScroll(), b.didScroll = !1
          }
          var b = this;
          this.adapter.on("scroll.waypoints", function () {
              (!b.didScroll || e.isTouch) && (b.didScroll = !0, e.requestAnimationFrame(a))
          })
      }, b.prototype.handleResize = function () {
          e.Context.refreshAll()
      }, b.prototype.handleScroll = function () {
          var a = {},
            b = {
                horizontal: {
                    newScroll: this.adapter.scrollLeft(),
                    oldScroll: this.oldScroll.x,
                    forward: "right",
                    backward: "left"
                },
                vertical: {
                    newScroll: this.adapter.scrollTop(),
                    oldScroll: this.oldScroll.y,
                    forward: "down",
                    backward: "up"
                }
            };
          for (var c in b) {
              var d = b[c],
                e = d.newScroll > d.oldScroll,
                f = e ? d.forward : d.backward;
              for (var g in this.waypoints[c]) {
                  var h = this.waypoints[c][g],
                    i = d.oldScroll < h.triggerPoint,
                    j = d.newScroll >= h.triggerPoint,
                    k = i && j,
                    l = !i && !j;
                  (k || l) && (h.queueTrigger(f), a[h.group.id] = h.group)
              }
          }
          for (var m in a) a[m].flushTriggers();
          this.oldScroll = {
              x: b.horizontal.newScroll,
              y: b.vertical.newScroll
          }
      }, b.prototype.innerHeight = function () {
          return this.element == this.element.window ? e.viewportHeight() : this.adapter.innerHeight()
      }, b.prototype.remove = function (a) {
          delete this.waypoints[a.axis][a.key], this.checkEmpty()
      }, b.prototype.innerWidth = function () {
          return this.element == this.element.window ? e.viewportWidth() : this.adapter.innerWidth()
      }, b.prototype.destroy = function () {
          var a = [];
          for (var b in this.waypoints)
              for (var c in this.waypoints[b]) a.push(this.waypoints[b][c]);
          for (var d = 0, e = a.length; e > d; d++) a[d].destroy()
      }, b.prototype.refresh = function () {
          var a, b = this.element == this.element.window,
            c = b ? void 0 : this.adapter.offset(),
            d = {};
          this.handleScroll(), a = {
              horizontal: {
                  contextOffset: b ? 0 : c.left,
                  contextScroll: b ? 0 : this.oldScroll.x,
                  contextDimension: this.innerWidth(),
                  oldScroll: this.oldScroll.x,
                  forward: "right",
                  backward: "left",
                  offsetProp: "left"
              },
              vertical: {
                  contextOffset: b ? 0 : c.top,
                  contextScroll: b ? 0 : this.oldScroll.y,
                  contextDimension: this.innerHeight(),
                  oldScroll: this.oldScroll.y,
                  forward: "down",
                  backward: "up",
                  offsetProp: "top"
              }
          };
          for (var f in a) {
              var g = a[f];
              for (var h in this.waypoints[f]) {
                  var i, j, k, l, m, n = this.waypoints[f][h],
                    o = n.options.offset,
                    p = n.triggerPoint,
                    q = 0,
                    r = null == p;
                  n.element !== n.element.window && (q = n.adapter.offset()[g.offsetProp]), "function" == typeof o ? o = o.apply(n) : "string" == typeof o && (o = parseFloat(o), n.options.offset.indexOf("%") > -1 && (o = Math.ceil(g.contextDimension * o / 100))), i = g.contextScroll - g.contextOffset, n.triggerPoint = q + i - o, j = p < g.oldScroll, k = n.triggerPoint >= g.oldScroll, l = j && k, m = !j && !k, !r && l ? (n.queueTrigger(g.backward), d[n.group.id] = n.group) : !r && m ? (n.queueTrigger(g.forward), d[n.group.id] = n.group) : r && g.oldScroll >= n.triggerPoint && (n.queueTrigger(g.forward), d[n.group.id] = n.group)
              }
          }
          return e.requestAnimationFrame(function () {
              for (var a in d) d[a].flushTriggers()
          }), this
      }, b.findOrCreateByElement = function (a) {
          return b.findByElement(a) || new b(a)
      }, b.refreshAll = function () {
          for (var a in d) d[a].refresh()
      }, b.findByElement = function (a) {
          return d[a.waypointContextKey]
      }, window.onload = function () {
          f && f(), b.refreshAll()
      }, e.requestAnimationFrame = function (b) {
          var c = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || a;
          c.call(window, b)
      }, e.Context = b
  }(),
  function () {
      "use strict";

      function a(a, b) {
          return a.triggerPoint - b.triggerPoint
      }

      function b(a, b) {
          return b.triggerPoint - a.triggerPoint
      }

      function c(a) {
          this.name = a.name, this.axis = a.axis, this.id = this.name + "-" + this.axis, this.waypoints = [], this.clearTriggerQueues(), d[this.axis][this.name] = this
      }
      var d = {
          vertical: {},
          horizontal: {}
      },
        e = window.Waypoint;
      c.prototype.add = function (a) {
          this.waypoints.push(a)
      }, c.prototype.clearTriggerQueues = function () {
          this.triggerQueues = {
              up: [],
              down: [],
              left: [],
              right: []
          }
      }, c.prototype.flushTriggers = function () {
          for (var c in this.triggerQueues) {
              var d = this.triggerQueues[c],
                e = "up" === c || "left" === c;
              d.sort(e ? b : a);
              for (var f = 0, g = d.length; g > f; f += 1) {
                  var h = d[f];
                  (h.options.continuous || f === d.length - 1) && h.trigger([c])
              }
          }
          this.clearTriggerQueues()
      }, c.prototype.next = function (b) {
          this.waypoints.sort(a);
          var c = e.Adapter.inArray(b, this.waypoints),
            d = c === this.waypoints.length - 1;
          return d ? null : this.waypoints[c + 1]
      }, c.prototype.previous = function (b) {
          this.waypoints.sort(a);
          var c = e.Adapter.inArray(b, this.waypoints);
          return c ? this.waypoints[c - 1] : null
      }, c.prototype.queueTrigger = function (a, b) {
          this.triggerQueues[b].push(a)
      }, c.prototype.remove = function (a) {
          var b = e.Adapter.inArray(a, this.waypoints);
          b > -1 && this.waypoints.splice(b, 1)
      }, c.prototype.first = function () {
          return this.waypoints[0]
      }, c.prototype.last = function () {
          return this.waypoints[this.waypoints.length - 1]
      }, c.findOrCreate = function (a) {
          return d[a.axis][a.name] || new c(a)
      }, e.Group = c
  }(),
  function () {
      "use strict";

      function a(a) {
          this.$element = b(a)
      }
      var b = window.jQuery,
        c = window.Waypoint;
      b.each(["innerHeight", "innerWidth", "off", "offset", "on", "outerHeight", "outerWidth", "scrollLeft", "scrollTop"], function (b, c) {
          a.prototype[c] = function () {
              var a = Array.prototype.slice.call(arguments);
              return this.$element[c].apply(this.$element, a)
          }
      }), b.each(["extend", "inArray", "isEmptyObject"], function (c, d) {
          a[d] = b[d]
      }), c.adapters.push({
          name: "jquery",
          Adapter: a
      }), c.Adapter = a
  }(),
  function () {
      "use strict";

      function a(a) {
          return function () {
              var c = [],
                d = arguments[0];
              return a.isFunction(arguments[0]) && (d = a.extend({}, arguments[1]), d.handler = arguments[0]), this.each(function () {
                  var e = a.extend({}, d, {
                      element: this
                  });
                  "string" == typeof e.context && (e.context = a(this).closest(e.context)[0]), c.push(new b(e))
              }), c
          }
      }
      var b = window.Waypoint;
      window.jQuery && (window.jQuery.fn.waypoint = a(window.jQuery)), window.Zepto && (window.Zepto.fn.waypoint = a(window.Zepto))
  }(),
  function (a) {
      "use strict";
      a.fn.cycle.transitions.scrollHorzRight = {
          before: function (b, c, d, e) {
              a(c).css("left", "auto"), b.API.stackSlides(c, d, e);
              var f = b.container.css("overflow", "hidden").width();
              b.cssBefore = {
                  right: e ? f : -f,
                  top: 0,
                  left: "auto",
                  opacity: 1,
                  visibility: "visible",
                  display: "block"
              }, b.cssAfter = {
                  zIndex: b._maxZ - 2,
                  right: 0,
                  left: "auto"
              }, b.animIn = {
                  right: 0
              }, b.animOut = {
                  right: e ? -f : f
              }
          }
      }
  }(jQuery),
  function (a) {
      "use strict";
      a.fn.cycle.transitions.scrollVertUp = {
          before: function (b, c, d, e) {
              b.API.stackSlides(b, c, d, e);
              matchMedia("(max-width: 768px)");
              if (Modernizr.cssanimations) {
                  var f = "animated bigHeaderSlideUpOut";
                  a(c).find(".bh__background").length && a(c).find(".bh__background").removeClass("slideInUpBig").addClass("animated slideOutUpBig"),
                  a(c).find(".bh__content").length && (a(c).find(".bh__subtitle .line").removeClass("bigHeaderSlideUpIn").addClass(f),
                  a(c).find(".bh__title .line").removeClass("bigHeaderSlideUpIn").addClass(f)),

                  a(d).find(".bh__background ").length && a(d).find(".bh__background").addClass("animated slideInUpBig"),
                  a(d).find(".bh__content ").length && (a(d).find(".bh__subtitle .line").addClass("animated bigHeaderSlideUpIn"),
                  a(d).find(".bh__title .line").addClass("animated bigHeaderSlideUpIn"))
              } else {
                  var g = b.container.css("overflow", "hidden").height();
                  b.cssBefore = {
                      bottom: e ? -g : g,
                      top: e ? g : -g,
                      left: 0,
                      opacity: 1,
                      display: "block",
                      visibility: "visible"
                  }, b.animIn = {
                      bottom: e ? -g : g,
                      top: 0
                  }, b.animOut = {
                      bottom: e ? g : -g,
                      top: e ? -g : g
                  }
              }
          },
          after: function (b, c) {
              var d = "animated bigHeaderSlideUpOut slideOutUpBig";
              a(c).find(".bh__background").removeClass(d), a(c).find(".bh__title .line").removeClass(d)
          }
      }
  }(jQuery);
var MSI = MSI || {};

var MSI = MSI || {},
  lastScrollTop = 0,
  scrollDelta = 100,
  trackScrolling = !1;
jQuery(document).ready(function (a) {
    Modernizr.svg || a('img[src$=".svg"]').each(function () {
        a(this).attr("src", a(this).attr("src").replace(".svg", ".png"))
    }), document.addEventListener("loadeddata", function (b) {
        c.parents(".bh__video .bh__background").removeClass("lazyload").addClass("lazyloaded").css("background-image", "none")
    }, !0)
});