$(document).ready(function () {
    // scroll to top on load
    //$('body, html').scrollTop(0);

    // search overlay functionality
    $('.vocus-search .search-icon').click(function (e) {
        e.preventDefault();
        $('body').addClass('stop-scrolling');
        $('.search-section').addClass('open');
        setTimeout(function () {
            $('.search-section .searchbox').focus();
        }, 100);        
    });

    function closeSearchOverlay() {
        $('body').removeClass('stop-scrolling');
        $('.search-section').removeClass('open');
    }

    $('.close-btn').click(function (e) {
        e.preventDefault();
        closeSearchOverlay();
    });

    // close search overlay on esc key
    $(document).keyup(function (e) {
        if (e.keyCode === 27) {
            closeSearchOverlay();
        }
    });

    // close search overlay on document click except input
    $(document).click(function (event) {
        if (!$(event.target).closest('.vocus-search .search-icon').length && !$(event.target).closest('.search-section .input-group').length) {
            if ($('.search-section.open').is(":visible")) {
                closeSearchOverlay();
            }
        }
    })


    
    //brand expand section

    $('.gallery-trigger').click(function (e) {
        e.preventDefault();
        var parentExpander = $(this).parents('.gallery-item'),
            headerContainerHeight = $('.main-header-cntr').innerHeight();

        parentExpander.css({ "max-height": "auto" });
        $('.gallery-item.active').not(parentExpander).find(".gallery-trigger-close").click();

        setTimeout(function () {
            $('body, html').stop().animate({
                scrollTop: parentExpander.find('.gallery-expander').offset().top - headerContainerHeight
            }, 500);
            return false;
        }, 500);
        return false;
    });

    (function (global, $) {
        $('.initialization , .brands').imagelistexpander({
            prefix: "gallery-"
        });
    })(this, jQuery)


    // Scroll to top
	$('.to-top-cntr a').on('click', function (e) {
	    e.preventDefault();
	    $('html, body').stop().animate({
            scrollTop: 0
	    }, 700);
	});

    // Set parent height 
	//if ($('.page__main-content').length > 0) {
	    //var parentHeight = $('.page__main-content').outerHeight(true);
	    //var absoluteHeight = $('.full-width-content').outerHeight(true);
	    //var finalHeight = parentHeight + absoluteHeight;
	    //setTimeout(function () {
	       // $('.page__main-content').css({ 'height': finalHeight });
	    //}, 1000);	    
    //}


    // accordion 
	var acc = document.getElementsByClassName("accordion");
	var i;

	for (i = 0; i < acc.length; i++) {
	    acc[i].onclick = function () {
	        this.classList.toggle("active");
	        var panel = this.nextElementSibling;
	        if (panel.style.maxHeight) {
	            panel.style.maxHeight = null;
	        } else {
	            panel.style.maxHeight = panel.scrollHeight + 'px';
	        }
	    }
	}

    // open first accordion on page load
	$(".accordion-wrap button:first-child").trigger('click');


    // Main navigation   
	if ($('.navbar-bold').length > 0) {
	    var navOverlay = $('.overlay_container_menu');

	    // navigation hover show overlay
	    function navHoverOverlay() {	        
	        $('ul.navbar-bold > li').hover(function () {
	            navOverlay.addClass('open');
	        }, function () {
	            navOverlay.removeClass('open');
	        });	            
	    }
	    navHoverOverlay();

	    // set max-height for navigation in mobile
	    var windowWidth = $(window).innerWidth(),
	        navParent = $('.navigation-cntr');
	    
	    function navMaxHeight() {
	        var navBar = $('.navbar-bold'),
                headerHeight = $('.main-header-cntr').innerHeight();
	        if (windowWidth <= 800) {
	            navBar.css({ 'max-height': $(window).innerHeight() - headerHeight });
	        } else {
	            navBar.css({ 'max-height': 'inherit' });
	        }
	    }
	    navMaxHeight();
	    window.addEventListener('resize', navMaxHeight);

	    // stick navigation on scroll
	    function stickNav() {
	        $(window).scroll(function () {
	            var scroll = $(window).scrollTop(),
	                headerContainer = $('.main-header-cntr'),
	                headerContainerHeight = $('.main-header-cntr').innerHeight(),
	                mainBody = $('.main');

	            if (scroll > 0) {
	                headerContainer.addClass('stick');
	                mainBody.css('padding-top', headerContainerHeight);
	            } else {
	                headerContainer.removeClass('stick');
	                mainBody.css('padding-top', 0);
	            }
	        });
	    }
	    stickNav();
	    window.addEventListener('resize load', stickNav);

        // toggle navbar
	    function toggleNav() {
	        $('.hamburger-icon').on('click', function (e) {
	            e.preventDefault();
	            $(this).toggleClass('active');
	            $(this).parents(navParent).find('.navbar-bold').slideToggle();
	            $('.overlay_container_menu_mobile').toggleClass('open');
	        });
	    }
	    toggleNav();

	    // close navigation on overlay tap in mobile
	    function closeNavOnOverlay() {
	        $('.overlay_container_menu_mobile').on('click', function () {
	            $(this).removeClass('open');
	            $('.hamburger-icon').removeClass('active');
	            $('.navbar-bold').slideUp();
	        });
	    }
	    closeNavOnOverlay();

	    // add downArrow which has child elements
	    function addDownArrow() {
	        if ($('.toggle-sub-nav').length <= 0) {
	            $('.nav-dropdown').closest('.has-child').find('> a').append('<i class="fa fa-angle-down toggle-sub-nav mobile"></i>');
	            $('.mega-menu-content.investorsMainPage .drop__child-links ul .child-nav-column > a').append('<i class="fa fa-angle-down toggle-sub-nav mobile"></i>');
	        }
	        if ($(window).innerWidth() <= 800) {
	            var hasChild = $('.has-child');	            
	            $('.navbar-light li').addClass('topnav-item');
	        } else {
	            $('.navbar-light li').removeClass('topnav-item');
	        }
	    }
	    addDownArrow();
	    window.addEventListener('resize', addDownArrow);

	    // move top navigation to dropdown in mobile
	    function moveNav() {
	        var topNav = $('.navbar-light'),
                mainNav = $('.navbar-bold'),
	            topNavItems = topNav.find('li'),
                mainNavTopItems = mainNav.find('li.topnav-item');
	        if ($(window).innerWidth() <= 800) {	            
	            $(topNavItems).clone().appendTo(mainNav);
	            topNavItems.remove();
	        } else {
	            $(mainNavTopItems).clone().appendTo(topNav);
	            mainNavTopItems.remove();
	        }
	    }
	    moveNav();
	    window.addEventListener('resize', moveNav);

        // toggle sub nav
	    function toggleSubNav() {
	        $('.navbar-bold > li.has-child > a .toggle-sub-nav').on('click', function (e) {
	            e.preventDefault();
	            $(this).closest('a').toggleClass('active');
	            $(this).closest('li.has-child').find('.nav-dropdown').slideToggle();
	        });

	        $('.mega-menu-content.investorsMainPage .drop__child-links ul .child-nav-column > a .toggle-sub-nav').on('click', function (e) {
	            e.preventDefault();
	            $(this).parent('a').toggleClass('active');
	            $(this).parents('li.child-nav-column').find('.sublevel').slideToggle();
	        });
	    }
	    toggleSubNav();
	}

	function formatNumber() {
	    var nStr = $(".pricevolume").text();
	    nStr += '';
	    x = nStr.split('.');
	    x1 = x[0];
	    x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while (rgx.test(x1)) {
	        x1 = x1.replace(rgx, '$1' + ',' + '$2');
	    }
	    nStr = x1 + x2;
	    $(".pricevolume").text(nStr);
	}
	formatNumber();

});
