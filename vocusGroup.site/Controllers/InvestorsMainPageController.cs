﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using VocusGroup.site.Models.Generated.PublishedContentModels;
using VocusGroup.site.Helper;
using System.Configuration;

namespace VocusGroup.site.Controllers
{
    public class InvestorsMainPageController : RenderMvcController
    {
        [HttpGet]
        public override ActionResult Index(RenderModel model)
        {
            InvestorsMainPage viewModel = new InvestorsMainPage(model.Content);
            HttpWebRequest request
             = WebRequest.Create(Helper.WebHelper.GetConfiguration(ApplicationConstants.ApplicationConstants.KeyXmlUrl)) as HttpWebRequest;

            //  Get response
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                //  Load data into a dataset
                DataSet dsParsedData = new DataSet();
                dsParsedData.ReadXml(response.GetResponseStream());
                if(dsParsedData.Tables.Contains("Stock_Quote"))
                {
                    viewModel.LatestSharePrice = dsParsedData.Tables[2].Rows[0][1].ToString();
                    viewModel.ShareInformation = dsParsedData.Tables[2].Rows[0][2].ToString();
                    var changePercent = 0.00;
                    var previousClose = dsParsedData.Tables[2].Rows[0][11].ToString();
                    var prevClose = Convert.ToDouble(previousClose);
                    if (prevClose != 0.00)
                    {
                        changePercent = (Convert.ToDouble(viewModel.ShareInformation) / Convert.ToDouble(previousClose)) * 100;
                    }
                    if (viewModel.ShareInformation.Contains("-"))
                    {
                        viewModel.ShareInformation = viewModel.ShareInformation.Replace("-", "-$");
                    }
                    else
                    {
                        viewModel.ShareInformation = "$" + viewModel.ShareInformation;
                    }
                    viewModel.ShareInfoPercentage = changePercent.ToString();
                    viewModel.ShareInfoPercentage = Math.Round(changePercent, 2) + "%";
                    CultureInfo provider = CultureInfo.InvariantCulture;
                    DateTime date = Convert.ToDateTime(dsParsedData.Tables[3].Rows[0][2]);
                    viewModel.LastTrade = String.Format("{0:HH:mm dd MMM yyyy}", date);
                }
                else 
                {
                    viewModel.LastTrade = "Error in extracting data.";
                }
               

            }

            return CurrentTemplate(viewModel);
        }

    }
}