﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace VocusGroup.site.Controllers
{
    public class HomeController : RenderMvcController
    {
        // GET: Home
        public override ActionResult Index(RenderModel model)
        {
            return CurrentTemplate(model);
        }
    }
}