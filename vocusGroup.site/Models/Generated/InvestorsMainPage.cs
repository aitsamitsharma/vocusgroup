﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace VocusGroup.site.Models.Generated.PublishedContentModels
{
    public partial class InvestorsMainPage
    {
        public string LatestSharePrice { get; set; }

        public string ShareInformation { get; set; }

        public string ShareInfoPercentage { get; set; }

        public string LastTrade { get; set; }

        public string Currency { get; set; }
    }
}