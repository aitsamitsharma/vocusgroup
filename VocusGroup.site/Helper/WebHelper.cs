﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace VocusGroup.site.Helper
{
    public static class WebHelper
    {
        /// <summary>
        /// <para>
        /// Read string configuration value.
        /// </para>
        /// </summary>
        /// <param name="name">
        /// The configuration name.
        /// </param>
        /// <exception cref="ConfigurationErrorsException">
        /// If value is null or empty string.
        /// </exception>
        public static string GetConfiguration(string name)
        {
            try
            {
                var value = ConfigurationManager.AppSettings[name];
                if (string.IsNullOrEmpty(value))
                {
                    throw new ConfigurationErrorsException(
                        string.Format("Instance property '{0}' wasn't configured properly.", name));
                }
                return value;
            }
            catch (Exception ex)
            {
                throw ex is ConfigurationErrorsException
                    ? ex
                    : new ConfigurationErrorsException(
                        string.Format("Instance property '{0}' wasn't configured properly.", name), ex);
            }
        }
    }
}