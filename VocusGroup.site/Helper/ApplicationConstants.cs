﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace VocusGroup.site.ApplicationConstants
{
    public static class ApplicationConstants
    {
        #region AppSetting key names

        /// <summary>
        /// Represents the key name for xml url.
        /// </summary>
        public const string KeyXmlUrl = "xmlUrl";

        /// <summary>
        /// Represents the key name for sharePriceUrl.
        /// </summary>
        public const string KeysharePriceUrl = "sharePriceUrl";

        /// <summary>
        /// Represents the key name for asxAnnouncementsUrl.
        /// </summary>
        public const string KeyasxAnnouncementsUrl = "asxAnnouncementsUrl";

        #endregion
    }
}