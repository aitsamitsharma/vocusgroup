﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using VocusGroup.site.Models.Generated.PublishedContentModels;

namespace VocusGroup.site.Controllers
{
    public class SharePricePageController : RenderMvcController
    {

        [HttpGet]
        public override ActionResult Index(RenderModel model)
        {
            SharePricePage viewModel = new SharePricePage(model.Content);
            HttpWebRequest request
             = WebRequest.Create(Helper.WebHelper.GetConfiguration(ApplicationConstants.ApplicationConstants.KeysharePriceUrl)) as HttpWebRequest;

            //  Get response
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                //  Load data into a dataset
                DataSet dsParsedData = new DataSet();
                dsParsedData.ReadXml(response.GetResponseStream());
                if (dsParsedData.Tables.Contains("Stock_Quote"))
                {
                    var changePercent = 0.00;
                    viewModel.Title = dsParsedData.Tables[2].Rows[0][23].ToString();
                    viewModel.ExchangeOne = dsParsedData.Tables[2].Rows[0][24].ToString();
                    viewModel.ExchangeTwo=dsParsedData.Tables[2].Rows[0][25].ToString();
                    viewModel.Price = dsParsedData.Tables[2].Rows[0][1].ToString();

                  
                    viewModel.Volume = dsParsedData.Tables[2].Rows[0][9].ToString();
                    viewModel.TodaysOpen = dsParsedData.Tables[2].Rows[0][10].ToString();
                    viewModel.PreviousClose = dsParsedData.Tables[2].Rows[0][11].ToString();
                    viewModel.Change = dsParsedData.Tables[2].Rows[0][2].ToString();
                    var prevClose = Convert.ToDouble(viewModel.PreviousClose);
                    if (prevClose != 0.00)
                    {
                         changePercent = (Convert.ToDouble(viewModel.Change) / Convert.ToDouble(viewModel.PreviousClose)) * 100;
                    }
                    viewModel.ChangePercent = Math.Round(changePercent,2).ToString()+'%';
                    viewModel.Bid = (dsParsedData.Tables[2].Rows[0][3].ToString());
                    viewModel.Ask = (dsParsedData.Tables[2].Rows[0][4].ToString());
                    viewModel.IntradayHigh = (dsParsedData.Tables[2].Rows[0][7].ToString());
                    viewModel.IntradayLow = (dsParsedData.Tables[2].Rows[0][8].ToString());
                    viewModel.FiftyTwoWeekHigh = (dsParsedData.Tables[2].Rows[0][12]).ToString();
                    viewModel.FiftyTwoWeekLow = (dsParsedData.Tables[2].Rows[0][13]).ToString();
                    viewModel.CurrencyType= (dsParsedData.Tables[2].Rows[0][26]).ToString();
                    DateTime date = Convert.ToDateTime(dsParsedData.Tables[3].Rows[0][2]);
                    viewModel.DataAsOf = String.Format("{0:dd.MM.yyyy h.mmtt ET}", date);
                }
                else
                {
                    viewModel.ExchangeOne = "Error in extracting data.";
                }


            }

            return CurrentTemplate(viewModel);
        }

    }
}
