﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;
using VocusGroup.site.Models.Generated;
using VocusGroup.site.Models.Generated.PublishedContentModels;

namespace VocusGroup.site.Controllers
{
    public class InvestorsReportContentPageController : RenderMvcController
    {
        [HttpGet]
        // GET: InvestorsReportContentPage
        public override ActionResult Index(RenderModel model)
        {

            HttpWebRequest request
           = WebRequest.Create(Helper.WebHelper.GetConfiguration(ApplicationConstants.ApplicationConstants.KeyasxAnnouncementsUrl)) as HttpWebRequest;
            InvestorsReportContentPage viewModel1 = new InvestorsReportContentPage(model.Content);
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                //  Load data into a dataset
                DataSet dsParsedData = new DataSet();
                dsParsedData.ReadXml(response.GetResponseStream());
                var investorSubMenuList = new List<Models.Generated.PublishedContentModels.InvestorsReportContentPage>();
                 if (dsParsedData.Tables.Contains("NewsRelease") && dsParsedData.Tables.Contains("PDF") && dsParsedData.Tables.Contains("Date"))
                {

                    var table1 = new System.Data.DataTable();
                    table1 = dsParsedData.Tables[2];
                    var table2 = new System.Data.DataTable();
                    table2 = dsParsedData.Tables[3];
                    var table3 = new System.Data.DataTable();
                    table3 = dsParsedData.Tables[4];
                 
                    foreach (System.Data.DataRow item in table1.Rows)
                    {
                        InvestorsReportContentPage viewModel = new InvestorsReportContentPage(model.Content);
                        viewModel.PdfMainTitle = item["Title"].ToString();
                        viewModel.NewsReleaseId = Convert.ToInt32(item["NewsRelease_Id"]);
                        viewModel.PdfUrl = item["ExternalURL"].ToString();
                        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(viewModel.PdfUrl);
                        req.Method = "HEAD";
                        HttpWebResponse resp = (HttpWebResponse)(req.GetResponse());
                        decimal len = resp.ContentLength;
                        long contentInKb = Convert.ToInt64((Math.Ceiling(len / 1024)));
                        viewModel.Size = Convert.ToInt32(contentInKb);
                        foreach (System.Data.DataRow item1 in table2.Rows)
                        {
                            viewModel.NewsReleaseIdSecond = Convert.ToInt32(item1["NewsRelease_Id"]);
                            if (viewModel.NewsReleaseId == viewModel.NewsReleaseIdSecond)
                            {
                                viewModel.PdfTitle = item1["PDFTitle"].ToString();
                                foreach (System.Data.DataRow item2 in table3.Rows)
                                {
                                    var NewsReleaseIdThird = Convert.ToInt32(item2["NewsRelease_Id"]);
                                    if (viewModel.NewsReleaseId == NewsReleaseIdThird)
                                    {
                                        CultureInfo provider = CultureInfo.InvariantCulture;
                                        var date = item2["Date"].ToString();
                                        string format = "yyyyMMdd";
                                        var date1 = DateTime.ParseExact(date, format, provider);
                                        viewModel.DisplayDate = String.Format("{0:MMM dd, yyyy}", date1);
                                        viewModel.AsxYear= String.Format("{0:yyyy}", date1);
                                    }
                                }
                            }
                        }
                        investorSubMenuList.Add(viewModel);

                    }
                 
                    viewModel1.InvestorSubMenuList = investorSubMenuList;

                }
                else
                {
                    viewModel1.AsxYear = "Not Available";
                    investorSubMenuList.Add(viewModel1);
                    viewModel1.InvestorSubMenuList=investorSubMenuList;
                }


            }

            return CurrentTemplate(viewModel1);
        }
    }
}