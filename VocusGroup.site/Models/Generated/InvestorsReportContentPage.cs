﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VocusGroup.site.Models.Generated.PublishedContentModels;

namespace VocusGroup.site.Models.Generated.PublishedContentModels
{
    public partial class InvestorsReportContentPage
    {
        public string AsxYear { get; set; }

        public string PdfMainTitle { get; set; }

        public string DisplayDate { get; set; }

        public string PdfTitle { get; set; }

        public string Title { get; set; }

        public int Size { get; set; }

        public List<InvestorsReportContentPage> InvestorSubMenuList { get; set; }

        public int NewsReleaseId { get; set; }

        public int NewsReleaseIdSecond { get; set; }

        public string PdfUrl { get; set; }

    }
}