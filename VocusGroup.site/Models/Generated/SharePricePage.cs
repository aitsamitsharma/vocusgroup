﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VocusGroup.site.Models.Generated.PublishedContentModels
{
    public partial class SharePricePage
    {
        public string Title { get; set; }
        public string ExchangeOne { get; set; }

        public string ExchangeTwo { get; set; }

        public string  Price { get; set; }

        public string ChangePercent { get; set; }

        public string Volume { get; set; }

        public string TodaysOpen { get; set; }

        public string PreviousClose { get; set; }

        public string Bid { get; set; }

        public string Ask { get; set; }

        public string IntradayHigh { get; set; }

        public string IntradayLow { get; set; }

        public string FiftyTwoWeekHigh { get; set; }

        public string FiftyTwoWeekLow { get; set; }

        public string DataAsOf { get; set; }

        public string Change { get; set; }

        public string CurrencyType { get; set; }
    }
}