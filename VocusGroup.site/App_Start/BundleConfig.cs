﻿using System.Web.Optimization;
using Umbraco.Core.Logging;

namespace VocusGroup.site.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/css").Include(
                "~/css/bootstrap.min.css",
                "~/css/fonts.css", 
                "~/css/style.css",
                "~/css/responsive.css"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/Scripts/jquery-3.1.1.min.js", 
                "~/Scripts/jquery.imagelistexpander.js",
                "~/Scripts/slider.js",
                "~/Scripts/custom.js"));

            //Comment this out to control this setting via web.config compilation debug attribute
            //BundleTable.EnableOptimizations = true;
        }
    }
}